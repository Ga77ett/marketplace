//
//  MPLAppearance.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 19/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLAppearance: NSObject {
    class func addShadow(view: UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowPath =
            UIBezierPath(roundedRect: view.bounds,
                         cornerRadius: view.layer.cornerRadius).cgPath
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 5.0
    }
}
