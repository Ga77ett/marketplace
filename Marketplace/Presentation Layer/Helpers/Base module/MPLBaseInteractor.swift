//
//  MPLBaseIneractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation
import Alamofire

class MPLBaseInteractor: NSObject {
    
    // MARK: - Private vars & lets
    
    internal let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())

}
