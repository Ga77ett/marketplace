//
//  MPLValidation.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 04/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLValidation: NSObject {
    
    class func validate(email: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: email)
    }
}
