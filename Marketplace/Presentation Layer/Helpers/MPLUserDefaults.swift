//
//  MPLUserDefaults.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 29/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

let kMPLDefaultCityID   = "keyMPLDefaultCityID"
let kMPLDefaultCityName = "keyMPLDefaultCityName"
let kMPLUserToken       = "keyMPLUserToken"
