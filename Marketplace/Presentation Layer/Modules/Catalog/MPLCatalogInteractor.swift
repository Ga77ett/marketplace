//
//  MPLCatalogInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCatalogInteractor: MPLBaseInteractor, MPLCatalogInteractorInput {
        
    // MARK: Public vars & lets
    
    public var output: MPLCatalogInteractorOutput?
    

    // MARK: - MPLCatalogInteractorInput
    
    func downloadCatalog() {
        apiManager.call(type: RequestItemsType.categories, params: ["visible": "true", "get_images": "true",
                                                                    "sort_by": "position", "group_by_level": "true"]) {
                                                                        (res: Swift.Result<APIResponse.CategoriesList, AlertMessage>) in
                                                                        switch res {
                                                                        case .success(let categories):
                                                                            self.output?.catalogWasDownloaded(catalog: categories)
                                                                            break
                                                                            
                                                                        case .failure(let message):
                                                                            print(message.body)
                                                                            break
                                                                        }
        }
    }
    
    func downloadCatalog(category: APIResponse.Category) {
        apiManager.call(type: RequestItemsType.categories, params: ["visible": "true", "get_images": "true",
                                                                    "sort_by": "position", "parent_category_id": "\(category.categoryID)"]) {
                                                                        (res: Swift.Result<APIResponse.CategoriesList, AlertMessage>) in
                                                                        switch res {
                                                                        case .success(let categories):
                                                                            let newCatalog = self.createOverallCatalogItem(catalog: categories,
                                                                                                                           parentCategory: category)
                                                                            self.output?.catalogWasDownloaded(catalog: newCatalog)
                                                                            break
                                                                            
                                                                        case .failure(let message):
                                                                            print(message.body)
                                                                            break
                                                                        }
        }
    }
    
    
    // MARK: - Private
    
    private func createOverallCatalogItem(catalog: APIResponse.CategoriesList, parentCategory: APIResponse.Category) -> APIResponse.CategoriesList {
        var mCatalog = catalog
        
        let overall = APIResponse.Category.init(categoryID: parentCategory.categoryID,
                                                  parentID: "1",
                                                  name: "Всего товаров",
                                                  hasChildren: false,
                                                  productCount: String(parentCategory.subcatsCount),
                                                  subcatsCount: 0,
                                                  mainPair: nil)
        
        mCatalog.categories.insert(overall, at: 0)
        
        return mCatalog
    }
}
