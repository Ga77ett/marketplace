//
//  MPLCatalogViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCatalogViewController: UITableViewController, MPLCatalogViewInput {
    
    // MARK: - Public vars & lets
    
    public var output: MPLCatalogViewOutput?
    
    
    // MARK: - Private vars & lets
    
    private var dataSource: APIResponse.CategoriesList?
    
    
    // MARK: - Setup
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLCatalogAssembly.createModule(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewDidLoad()
    }

    private func setupInitialState() {
        setupTableView()
        setupSearchButton()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLCatalogCell.cellNibName(), bundle: nil),
                                  forCellReuseIdentifier: MPLCatalogCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }
    
    private func setupSearchButton() {
        let searchButton = UIButton(type: .custom)
        searchButton.frame = CGRect(x:0, y:0, width: 45, height:45)
        searchButton.setImage(UIImage(named: "Search_navbar_icon"), for: .normal)
        searchButton.setImage(UIImage(named: "Search_navbar_icon"), for: .highlighted)
        let backBarButtonItem = UIBarButtonItem.init(customView: searchButton)
        searchButton.addTarget(self, action: #selector(self.searchAction), for: .touchDown)
        navigationItem.rightBarButtonItem = backBarButtonItem
    }
    
    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.categories.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLCatalogCell.cellIdentifier(), for: indexPath) as! MPLCatalogCell

        cell.configure(catalog: (dataSource?.categories[indexPath.row])!)

        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLCatalogCell.cellHeight()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let category = dataSource?.categories[indexPath.row] else { return }
        
        output?.rowWasTapped(category: category)
    }

    
    // MARK: - MPLCatalogViewInput
    
    func setupCatalog(catalog: APIResponse.CategoriesList) {
        //guard let catalogName = catalog.categories.first?.mainPair.
        dataSource = catalog
        tableView.reloadData()
    }
    
    func setupTitle(title: String) {
        self.title = title
    }
    
    
    // MARK: - Action
    
    @objc func searchAction() {
        output?.searchTapped()
    }
}
