//
//  MPLCatalogPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCatalogPresenter: NSObject, MPLCatalogViewOutput, MPLCatalogInteractorOutput, MPLCatalogModuleConfigurator {
    
    // MARK: Public vars & lets
    
    public var view: MPLCatalogViewInput?
    public var interactor: MPLCatalogInteractorInput?
    public var router: MPLCatalogRouterInput?
    
    
    // MARK: Private vars & lets
    
    private var category: APIResponse.Category?
    
    // MARK: - MPLCatalogModuleConfigurator
    
    func configureModule(category: APIResponse.Category) {
        self.category = category
        view?.setupTitle(title: category.name)
    }
    

    // MARK: - MPLCatalogViewOutput
    
    func viewDidLoad() {
        guard let category = self.category else {
            interactor?.downloadCatalog()
            return
        }
        
        interactor?.downloadCatalog(category: category)
    }
    
    func rowWasTapped(category: APIResponse.Category) {
        if !category.hasChildren {
            router?.showProductListModule(id: category.categoryID, listName: category.name)
            
        } else {
            router?.showCatalogModule(category: category)
        }
    }
    
    func searchTapped() {
        router?.showSearch()
    }
    
    
    // MARK: - MPLCatalogInteractorOutput
    
    func catalogWasDownloaded(catalog: APIResponse.CategoriesList) {
        view?.setupCatalog(catalog: catalog)
    }
}
