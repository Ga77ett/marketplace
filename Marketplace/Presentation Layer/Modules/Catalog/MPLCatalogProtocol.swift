//
//  MPLCatalogProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLCatalogModuleConfigurator {
    func configureModule(category: APIResponse.Category)
}

protocol MPLCatalogViewOutput {
    func viewDidLoad()
    func rowWasTapped(category: APIResponse.Category)
    func searchTapped()
}

protocol MPLCatalogViewInput {
    func setupCatalog(catalog: APIResponse.CategoriesList)
    func setupTitle(title: String)
}

protocol MPLCatalogInteractorOutput {
    func catalogWasDownloaded(catalog: APIResponse.CategoriesList)
}

protocol MPLCatalogInteractorInput {
    func downloadCatalog()
    func downloadCatalog(category: APIResponse.Category)
}

protocol MPLCatalogRouterInput {
    func showCatalogModule(category: APIResponse.Category)
    func showProductListModule(id: String, listName: String)
    func showSearch()
}
