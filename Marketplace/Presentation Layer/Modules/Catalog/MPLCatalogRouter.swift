//
//  MPLCatalogRouter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCatalogRouter: MPLBaseRouter, MPLCatalogRouterInput {
    
    // MARK: - MPLCatalogRouterInput
    
    func showCatalogModule(category: APIResponse.Category) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: String(describing: MPLCatalogViewController.self)) as! MPLCatalogViewController
        let presenter: MPLCatalogPresenter = vc.output! as! MPLCatalogPresenter
        presenter.configureModule(category: category)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showProductListModule(id: String, listName: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: String(describing: MPLProductListViewController.self)) as! MPLProductListViewController
        let presenter: MPLProductListPresenter = vc.output! as! MPLProductListPresenter
        presenter.configureModule(id: id, listName: listName, type: .list)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showSearch() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLSearchViewController") as! MPLSearchViewController
            
            self.view?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
