//
//  MPLCatalogAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCatalogAssembly: NSObject {
    class func createModule(viewController: MPLCatalogViewController) {
        let presenter = MPLCatalogPresenter()
        let interactor = MPLCatalogInteractor()
        let router = MPLCatalogRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
