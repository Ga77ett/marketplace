//
//  MPLFiltersProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 15/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLFiltersModuleOutput {
    func checkProductCount(filters: APIResponse.Filters)
    func viewWillClose()
    func filtersWasAccepted()
}

protocol MPLFiltersModuleConfiguration {
    func configure(type: MPLFiltersInteractorRequestType, id: String, variantID: String, output: MPLFiltersModuleOutput)
}

protocol MPLFiltersViewOutput {
    func viewDidLoad()
    func variantsDidTapped(filter: APIResponse.Filters.Filter)
    func priceDidTapped(filter: APIResponse.Filters.Filter)
    func viewWillClose()
    func switchWasChanged()
    func acceptFilters()
}

protocol MPLFiltersViewInput {
    func configure(filters: APIResponse.Filters)
    func replaceFilter(filter: APIResponse.Filters.Filter)
}

protocol MPLFiltersInteractorOutput {
    func filterWasDownloaded(filters: APIResponse.Filters)
}

protocol MPLFiltersInteractorInput {
    func downloadFilters(type: MPLFiltersInteractorRequestType,
                         id: String, variantID: String)
}

protocol MPLFiltersRouterInput {
    func showVariants(filter: APIResponse.Filters.Filter, output: MPLSimpleSearchViewControllerOutput)
    func showPrice(filter: APIResponse.Filters.Filter, output: MPLPriceViewControllerOutput)
}

enum MPLFilterDisplayType: String {
    case radio
    case checkbox
    case price       = "input_from_to"
    case switchType  = "switch"
}
