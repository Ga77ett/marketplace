//
//  MPLFiltersInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 15/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

enum MPLFiltersInteractorRequestType: Int {
    case category   = 0
    case newest     = 1
    case sale       = 2
    case brands     = 3
    case search     = 4
}

class MPLFiltersInteractor: MPLBaseInteractor, MPLFiltersInteractorInput {

    // MARK: - Public vars & lets
    
    public var output: MPLFiltersInteractorOutput?
    
    
    // MARK: - MPLFiltersInteractorInput
    
    func downloadFilters(type: MPLFiltersInteractorRequestType,
                         id: String, variantID: String) {
        var requestDict: [String: String] = [:]
        switch type {
        case .category:
            requestDict = ["category_id" : id]
        case .newest:
            requestDict = ["special_type" : "newest"]
        case .sale:
            requestDict = ["special_type" : "on_sale"]
        case .brands:
            requestDict = ["feature_id" : id, "variant_id": variantID]
        case .search:
            requestDict = ["q" : id]
        }
        apiManager.call(type: RequestItemsType.filters,
                        params: requestDict) {
        (res: Swift.Result<APIResponse.Filters, AlertMessage>) in
            switch res {
            case .success(let filters):
                self.output?.filterWasDownloaded(filters: filters)
                break
            case .failure(let message):
                print(message.body)
                break
            }
        }
    }
}
