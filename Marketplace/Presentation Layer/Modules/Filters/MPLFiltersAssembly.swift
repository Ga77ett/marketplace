//
//  MPLFiltersAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 15/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLFiltersAssembly: NSObject {
    class func createModule(viewController: MPLFiltersViewController) {
        let presenter = MPLFiltersPresenter()
        let interactor = MPLFiltersInteractor()
        let router = MPLFiltersRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
