//
//  MPLFiltersRouter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 22/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLFiltersRouter: MPLBaseRouter, MPLFiltersRouterInput {

    // MARK: - MPLFiltersRouterInput
    
    func showVariants(filter: APIResponse.Filters.Filter, output: MPLSimpleSearchViewControllerOutput) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLSimpleSearchViewController") as! MPLSimpleSearchViewController
            controller.configure(filter: filter, output: output)
            self.view?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func showPrice(filter: APIResponse.Filters.Filter, output: MPLPriceViewControllerOutput) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLPriceViewController") as! MPLPriceViewController
            controller.configure(filter: filter, output: output)
            self.view?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
