//
//  MPLFilterViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 15/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLFiltersViewController: UIViewController, MPLFiltersViewInput, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Public vars & lets
    
    public var output: MPLFiltersViewOutput?
    public var data: APIResponse.Filters?
    public var waitingCount: Bool = false
    
    
    // MARK: - Private vars & lets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showButton: UIButton!
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLFiltersAssembly.createModule(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        setupInitialState()
        output?.viewDidLoad()
    }
    
    private func setupInitialState() {
        setupTableView()
        setupSearchButton()
        setupShowButton()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLFilterCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLFilterCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }
    
    private func setupSearchButton() {
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x:0, y:0, width: 24, height:24)
        closeButton.setImage(UIImage(named: "Close_icon"), for: .normal)
        closeButton.setImage(UIImage(named: "Close_icon"), for: .highlighted)
        let leftButtonItem = UIBarButtonItem.init(customView: closeButton)
        closeButton.addTarget(self, action: #selector(self.closeAction), for: .touchDown)
        navigationItem.leftBarButtonItem = leftButtonItem
    }
    
    private func setupShowButton() {
        showButton.layer.cornerRadius = 25
        showButton.clipsToBounds = true
        MPLAppearance.addShadow(view: showButton)
        
        showButton.addTarget(self, action: #selector(doneDidTapped), for: .touchDown)
    }
    
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.filters.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLFilterCell.cellIdentifier(), for: indexPath) as! MPLFilterCell
        guard let filter = data?.filters[indexPath.row] else { return cell }
        cell.configure(filter: filter)
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLFilterCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let filter = data?.filters[indexPath.row] else { return }
        
        if filter.displayType == MPLFilterDisplayType.checkbox.rawValue || filter.displayType == MPLFilterDisplayType.radio.rawValue {
            output?.variantsDidTapped(filter: filter)
            
        } else if filter.displayType == MPLFilterDisplayType.price.rawValue {
            output?.priceDidTapped(filter: filter)
            
        } else if filter.displayType == MPLFilterDisplayType.switchType.rawValue {
            if let variant = data?.filters[indexPath.row].variants?.first {
                if let row = data?.filters[indexPath.row].variants?.firstIndex(where: {$0.id == variant.id}) {
                    data?.filters[indexPath.row].variants?[row].selected = !variant.selected
                }
            }
            tableView.reloadData()
            output?.switchWasChanged()
        }
        
        waitingCount = true
    }
    
    
    // MARK: - Actions
    
    @objc func closeAction() {
        output?.viewWillClose()
        navigationController?.dismiss(animated: true, completion: nil)
        waitingCount = false
    }
    
    @objc func doneDidTapped() {
        output?.acceptFilters()
        navigationController?.dismiss(animated: true, completion: nil)
        waitingCount = false
    }
    
    
    // MARK: - Filters
    
    func configure(filters: APIResponse.Filters) {
        data = filters
        tableView.reloadData()
    }
    
    func replaceFilter(filter: APIResponse.Filters.Filter) {
        guard let filtersData = data?.filters else { return }
        
        if let row = filtersData.firstIndex(where: {$0.id == filter.id}) {
            self.data?.filters[row] = filter
        }
        
        tableView.reloadData()
    }
    
    func showCount(productCount: String) {
        let count = Int(productCount) ?? 0
        let formatString : String = NSLocalizedString("show_button_product_count",
                                                      comment:"")
        let resultString : String = String.localizedStringWithFormat(formatString, count)
        showButton.setTitle(resultString, for: .normal)
        waitingCount = false
    }
}
