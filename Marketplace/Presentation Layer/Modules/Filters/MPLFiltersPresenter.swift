//
//  MPLFiltersPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 15/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLFiltersPresenter: NSObject, MPLFiltersViewOutput, MPLFiltersInteractorOutput,
MPLFiltersModuleConfiguration, MPLSimpleSearchViewControllerOutput, MPLPriceViewControllerOutput {

    // MARK: Public vars & lets
    
    public var view: MPLFiltersViewInput?
    public var interactor: MPLFiltersInteractorInput?
    public var router: MPLFiltersRouter?
    public var output: MPLFiltersModuleOutput?
    
    
    // MARK: Public vars & lets
    
    private var type: MPLFiltersInteractorRequestType = .category
    private var id: String = ""
    private var variantID: String = ""
    
    // MARK: - MPLFiltersModuleConfiguration
    
    func configure(type: MPLFiltersInteractorRequestType, id: String, variantID: String, output: MPLFiltersModuleOutput) {
        self.type = type
        self.id = id
        self.variantID = variantID
        self.output = output
    }
    
    
    // MARK: - MPLFiltersViewOutput
    
    func viewDidLoad() {
        interactor?.downloadFilters(type: self.type, id: self.id, variantID: self.variantID)
    }
    
    func variantsDidTapped(filter: APIResponse.Filters.Filter) {
        router?.showVariants(filter: filter, output: self)
    }
    
    func viewWillClose() {
        output?.viewWillClose()
    }
    
    func priceDidTapped(filter: APIResponse.Filters.Filter) {
        router?.showPrice(filter: filter, output: self)
    }

    func switchWasChanged() {
        let controller = view as! MPLFiltersViewController
        output?.checkProductCount(filters: controller.data!)
    }
    
    func acceptFilters() {
        output?.filtersWasAccepted()
    }
    
    
    // MARK: - MPLFiltersInteractorOutput
    
    func filterWasDownloaded(filters: APIResponse.Filters) {
        view?.configure(filters: filters)
    }
    
    
    // MARK: MPLSimpleSearchViewControllerOutput
    
    func done(filter: APIResponse.Filters.Filter) {
        filtersWasChanged(filter: filter)
    }
    
    
    // MARK: - MPLPriceViewControllerOutput
    
    func priceWasSelected(filter: APIResponse.Filters.Filter) {
        filtersWasChanged(filter: filter)
    }
    
    
    // MARK: - Helper
    
    private func filtersWasChanged(filter: APIResponse.Filters.Filter) {
        view?.replaceFilter(filter: filter)
        let controller = view as! MPLFiltersViewController
        output?.checkProductCount(filters: controller.data!)
    }
}
