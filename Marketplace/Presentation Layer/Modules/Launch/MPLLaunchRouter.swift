//
//  MPLLaunchRouter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLLaunchRouter: MPLBaseRouter, MPLLaunchRouterInput {

    // MARK: - MPLLaunchRouterInput
    
    func showCities(output: MPLCitiesModuleOutput) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLCitiesViewController") as! MPLCitiesViewController
            let presenter = controller.output as! MPLCitiesPresenter
            presenter.configure(cityID: nil, output: output)
            let navVC = UINavigationController.init(rootViewController: controller)
            self.view?.navigationController?.present(navVC, animated: true, completion: nil)
        }
    }
    
    func showMain() {
        DispatchQueue.main.async {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "MPLTabbarController") as! UITabBarController
            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
    }
}
