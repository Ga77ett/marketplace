//
//  MPLLaunchInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import CoreLocation

class MPLLaunchInteractor: MPLBaseInteractor, MPLLaunchInteractorInput, CLLocationManagerDelegate {

    // MARK: - Public vars & lets
    
    public var output: MPLLaunchInteractorOutput?
    
    
    // MARK: - Private vars & lets
    
    private let locationManager = CLLocationManager()
    
    
    // MARK: - MPLLaunchInteractorInput
    
    func requestCoord() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
            
        case .denied, .restricted:
            output?.permissionRequested(autorized: false)
            return
            
        case .authorizedAlways, .authorizedWhenInUse:
            break
            
        @unknown default:
            return
        }
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    private func downloadCity(lat: Double, long: Double) {
        apiManager.call(type: RequestItemsType.cities,
                        params: ["lat" : lat, "lon" : long]) {
        (res: Swift.Result<APIResponse.Cities, AlertMessage>) in
            switch res {
            case .success(let cities):
                guard let city = cities.list.first else {
                    self.output?.permissionRequested(autorized: false)
                    return
                }
                self.output?.cityWasReceived(city: city)
                break
            case .failure(let message):
                print(message.body)
                break
            }
        }
    }
    
    func saveCity(id: String, name: String) {
        UserDefaults.standard.set(id, forKey: kMPLDefaultCityID)
        UserDefaults.standard.set(name, forKey: kMPLDefaultCityName)
        UserDefaults.standard.synchronize()
    }
    
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.last {
            locationManager.stopUpdatingLocation()
            self.downloadCity(lat: currentLocation.coordinate.latitude,
                              long: currentLocation.coordinate.longitude)
            print("Current location: \(currentLocation)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        output?.permissionRequested(autorized: false)
        print(error)
    }
}
