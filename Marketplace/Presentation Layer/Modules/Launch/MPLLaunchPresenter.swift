//
//  MPLLaunchPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLLaunchPresenter: NSObject, MPLLaunchViewOutput, MPLLaunchInteractorOutput, MPLCitiesModuleOutput {

    // MARK: Public vars & lets
    
    public var view: MPLLaunchViewInput?
    public var interactor: MPLLaunchInteractorInput?
    public var router: MPLLaunchRouter?
    
    
    // MARK: Public vars & lets
    
    private var city: APIResponse.Cities.City?
    

    // MARK: - MPLLaunchViewOutput
    
    func viewDidLoad() {
        if UserDefaults.standard.string(forKey: kMPLDefaultCityID) == nil {
            interactor?.requestCoord()
            
        } else {
           router?.showMain()
        }
    }
    
    func cityApproved(approved: Bool) {
        if approved {
            interactor?.saveCity(id: city!.id, name: city!.name)
            router?.showMain()
            
        } else {
            router?.showCities(output: self)
        }
    }
    
    
    // MARK: - MPLLaunchInteractorOutput
    
    func permissionRequested(autorized: Bool) {
        router?.showCities(output: self)
    }
    
    func cityWasReceived(city: APIResponse.Cities.City) {
        self.city = city
        view?.showSuggestion(cityName: city.name)
    }
    
    
    // MARK: - MPLCitiesModuleOutput
    
    func citySelected(cityID: String, name: String) {
        interactor?.saveCity(id: cityID, name: name)
        router?.showMain()
    }
}
