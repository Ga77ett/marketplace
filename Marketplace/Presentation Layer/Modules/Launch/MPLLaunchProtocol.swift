//
//  MPLLaunchProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLLaunchViewOutput {
    func viewDidLoad()
    func cityApproved(approved: Bool)
}

protocol MPLLaunchViewInput {
    func showSuggestion(cityName: String)
}

protocol MPLLaunchInteractorOutput {
    func permissionRequested(autorized: Bool)
    func cityWasReceived(city: APIResponse.Cities.City)
}

protocol MPLLaunchInteractorInput {
    func requestCoord()
    func saveCity(id: String, name: String)
}

protocol MPLLaunchRouterInput {
    func showCities(output: MPLCitiesModuleOutput)
    func showMain()
}
