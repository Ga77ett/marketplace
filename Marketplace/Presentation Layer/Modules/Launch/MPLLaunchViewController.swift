//
//  MPLLaunchViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLLaunchViewController: UIViewController, MPLLaunchViewInput {
    
    // MARK: - Public vars & lets
    
    public var output: MPLLaunchViewOutput?
    
    
    // MARK: - Private vars & lets
    
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLLaunchAssembly.createModule(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
    }
    
    
    // MARK: - MPLLaunchViewInput
    
    func showSuggestion(cityName: String) {
        let alert = UIAlertController(title: "Ваш город: \(cityName)",
            message: "Мы угадали?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Да", style: UIAlertAction.Style.default, handler: { action in
            self.output?.cityApproved(approved: true)
        }))
        alert.addAction(UIAlertAction(title: "Нет", style: UIAlertAction.Style.cancel, handler: { action in
            self.output?.cityApproved(approved: false)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
