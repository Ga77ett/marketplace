//
//  MPLLaunchAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLLaunchAssembly: NSObject {
    class func createModule(viewController: MPLLaunchViewController) {
        let presenter = MPLLaunchPresenter()
        let interactor = MPLLaunchInteractor()
        let router = MPLLaunchRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
