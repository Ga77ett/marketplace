//
//  MPLProductListRouter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProductListRouter: MPLBaseRouter, MPLProductListRouterInput {
    
    func showSearch() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLSearchViewController") as! MPLSearchViewController
            
            self.view?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func showFilters(type: MPLFiltersInteractorRequestType, id: String, variantID: String, output: MPLFiltersModuleOutput) -> MPLFiltersViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MPLFiltersViewController") as! MPLFiltersViewController
        let presenter = controller.output as! MPLFiltersPresenter
        presenter.configure(type: type, id: id, variantID: variantID, output: output)
        let navVC = UINavigationController.init(rootViewController: controller)
        
        self.view?.navigationController?.present(navVC, animated: true, completion: nil)
        
        return controller
    }
    
    func showFilters(vc: MPLFiltersViewController) {
        let navVC = UINavigationController.init(rootViewController: vc)
        self.view?.navigationController?.present(navVC, animated: true, completion: nil)
    }
    
    func showAuth() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Authorization", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLAuthViewController") as! MPLAuthViewController
            let navVC = UINavigationController.init(rootViewController: controller)
            
            self.view?.navigationController?.present(navVC, animated: true, completion: nil)
        }
    }
}
