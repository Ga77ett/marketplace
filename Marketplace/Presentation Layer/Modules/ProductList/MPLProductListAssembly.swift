//
//  MPLProductListAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProductListAssembly: NSObject {
    class func createModule(viewController: MPLProductListViewController) {
        let presenter = MPLProductListPresenter()
        let interactor = MPLProductListInteractor()
        let dataManager = MPLProductListDataManager()
        let router = MPLProductListRouter()
        
        viewController.output = presenter
        viewController.dataManager = dataManager
        dataManager.view = viewController
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
