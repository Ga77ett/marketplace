//
//  MPLProductListInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

enum MPLProductListInteractorRequestType: Int {
    case customProducts  = 0
    case specialProducts = 1
    case brands          = 2
}

class MPLProductListInteractor: MPLBaseInteractor, MPLProductListInteractorInput {

    // MARK: - Public vars & lets
    
    public var output: MPLProductListInteractorOutput?
    
    
    // MARK: - Private vars & lets
    
    private var page: Int           = 1
    private var id: String          = ""
    private var featureID: String   = ""
    private var sortString          = "popularity"
    private var sortDirection       = "desc"
    private let itemsPerPage        = 25
    private var saleList            = false
    private var requestType: MPLProductListInteractorRequestType = .customProducts
    private var selectedFilters: [APIResponse.Filters.Filter] = []
    
    
    // MARK: - MPLProductListInteractorInput
    
    func configure(listType: MPLProductListPresenterRequestType) {
        if listType == .newestProducts {
            sortString = "timestamp"
        }
    }
    
    func downloadProducts(id: String) {
        self.id = id
        requestType = .customProducts
        var requestKey = "cid"
        
        let presenter = output as! MPLProductListPresenter
        if presenter.requestType == .search {
            requestKey = "q"
            saveSearchText(text: id)
        }
        var params: [String: Any] = [requestKey: id, "subcats": "Y",
                                     "status": "A", "sort_by": sortString,
                                     "sort_order": sortDirection,
                                     "items_per_page": itemsPerPage, "page": page]
        
        if let token = UserDefaults.standard.object(forKey: kMPLUserToken) {
            params["client_token"] = token
        }
        
        apiManager.call(type: RequestItemsType.productList,
                        params: addFilters(toDict: params)) {
            (res: Swift.Result<APIResponse.ProductList, AlertMessage>) in
            switch res {
            case .success(let list):
                self.output?.listWasDownloaded(list: list)
                break

            case .failure(let message):
                print(message.body)
                break
            }
        }
    }
    
    func downloadSpecialProducts(isSale: Bool) {
        requestType = .specialProducts
        saleList = isSale
        let saleString = isSale ? "on_sale": "newest"
        var params: [String: Any] = ["type": saleString, "subcats": "Y",
                      "status": "A", "sort_order": sortDirection,
                      "sort_by": sortString, "items_per_page": itemsPerPage, "page": page]
        
        if let token = UserDefaults.standard.object(forKey: kMPLUserToken) {
            params["client_token"] = token
        }
        
        apiManager.call(type: RequestItemsType.specialProducts,
                        params: addFilters(toDict: params)) {
                                (res: Swift.Result<APIResponse.ProductList, AlertMessage>) in
                                switch res {
                                case .success(let list):
                                    self.output?.listWasDownloaded(list: list)
                                    break
                                case .failure(let message):
                                    print(message.body)
                                    break
            }
        }
    }
    
    func downloadProductsByBrand(variantID: String, featureID: String) {
        self.id = variantID
        self.featureID = featureID
        requestType = .brands
        var params: [String: Any] = ["variant_id": variantID, "feature_id": featureID,
                                     "subcats": "Y", "status": "A",
                                     "sort_by": sortString, "sort_order": sortDirection,
                                     "items_per_page": itemsPerPage, "page": page]
        
        if let token = UserDefaults.standard.object(forKey: kMPLUserToken) {
            params["client_token"] = token
        }
        
        apiManager.call(type: RequestItemsType.productList,
                        params: addFilters(toDict: params)) {
                                    (res: Swift.Result<APIResponse.ProductList, AlertMessage>) in
                                    switch res {
                                    case .success(let list):
                                        self.output?.listWasDownloaded(list: list)
                                        break
                                        
                                    case .failure(let message):
                                        print(message.body)
                                        break
                                    }
        }
    }
    
    func downloadMore() {
        page += 1
        
        switch requestType {
        case .customProducts:
            downloadProducts(id: self.id)
        case .specialProducts:
            downloadSpecialProducts(isSale: saleList)
        case .brands:
            downloadProductsByBrand(variantID: id, featureID: featureID)
        }
    }
    
    func doSortedRequest(type: MPLProductListViewControllerSortType) {
        page = 1
        
        switch type {
        case .popular:    self.sortString = "popularity"
        case .new:        self.sortString = "timestamp"
        case .priceDesc:
            self.sortString = "price"
            self.sortDirection = "desc"
        case .priceAsc:
            self.sortString = "price"
            self.sortDirection = "asc"
        }
        
        switch requestType {
        case .customProducts:
            downloadProducts(id: id)
        case .specialProducts:
            downloadSpecialProducts(isSale: saleList)
        case .brands:
            downloadProductsByBrand(variantID: id, featureID: featureID)
        }
    }
    
    func saveSearchText(text: String) {
        var data: [String] = []
        if let history = UserDefaults.standard.object(forKey: "kMPLSearchHistoryKey") as? [String] {
            data.append(contentsOf: history as [String])
        }
        
        var isEqual = false
        
        for historyText in data {
            if (historyText == text) {
                isEqual = true
            }
        }
        
        if !isEqual {
            if data.count == 10 {
                data.removeLast()
            }
            
            data.append(text)
            UserDefaults.standard.set(data, forKey: "kMPLSearchHistoryKey")
            UserDefaults.standard.synchronize()
        }
    }
    
    func addFilersToRequest(filters: APIResponse.Filters) {
        selectedFilters.removeAll()
        
        for filter in filters.filters {
            if filter.displayType == MPLFilterDisplayType.price.rawValue {
                if filter.selectedMin > 0 || filter.selectedMax > 0 {
                    selectedFilters.append(filter)
                }
                
            } else {
                if filter.variants != nil {
                    let selected = filter.variants!.filter { $0.selected }
                    if selected.count > 0 {
                        selectedFilters.append(filter)
                    }
                }
            }
        }
        
        switch requestType {
        case .customProducts:
            downloadProducts(id: id)
        case .specialProducts:
            downloadSpecialProducts(isSale: saleList)
        case .brands:
            downloadProductsByBrand(variantID: id, featureID: featureID)
        }
    }
    
    func selectedFiltersCount() -> Int {
        return selectedFilters.count
    }
    
    private func addFilters(toDict: [String: Any]) -> [String: Any] {
        var dict = toDict
        if selectedFilters.count > 0 {
            for filter in selectedFilters {
                var filterKeyStr = ""
                var filterValueStr = ""
                
                if filter.displayType == MPLFilterDisplayType.price.rawValue {
                    if filter.selectedMin > 0 {
                        dict["price_from"] = filter.selectedMin
                    }
                    
                    if filter.selectedMax > 0 {
                        dict["price_to"] = filter.selectedMax
                    }
                    
                } else {
                    guard let variants = filter.variants else { break }
                    for variant in variants {
                        if variant.selected {
                            if filterKeyStr.count == 0 {
                                filterKeyStr = "filter_variants[\(filter.id)][]"
                            }
                            
                            if filterValueStr.count == 0 {
                                filterValueStr = "\(variant.id)"
                                
                            } else {
                                filterValueStr += ",\(variant.id)"
                            }
                        }
                    }
                    
                    dict[filterKeyStr] = filterValueStr
                }
            }
        }
        
        return dict
    }
    
    func favouritesAdd(id: String) {
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else {
            self.output?.needLoginFirst()
            return
        }
        apiManager.call(type: RequestItemsType.wishlistAdd,
                        params: ["client_token" : token,
                                 "product_data[\(id)][product_id]" : id]) {
                            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                            switch res {
                            case .success(let response):
                                self.output?.favouritesAdded(added: response.errorCode == 0 ? true: false, message: response.errorMessage)
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
}

