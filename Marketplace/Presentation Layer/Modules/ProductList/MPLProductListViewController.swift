//
//  MPLProductListViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

enum MPLProductListViewControllerSortType: Int {
    case popular    = 0
    case new        = 1
    case priceAsc   = 2
    case priceDesc  = 3
}

class MPLProductListViewController: UIViewController, MPLProductListViewInput {
    
    // MARK: - Public vars & lets
    
    public var output: MPLProductListViewOutput?
    public var dataManager: MPLProductListDataManager?
    
    
    // MARK: - Private vars & lets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLProductListAssembly.createModule(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        setupInitialState()
        output?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func setupInitialState() {
        setupSearchButton()
        setupSortButton()
        setupCollectionView()
    }
    
    private func setupSearchButton() {
        let searchButton = UIButton(type: .custom)
        searchButton.frame = CGRect(x:0, y:0, width: 45, height:45)
        searchButton.setImage(UIImage(named: "Search_navbar_icon"), for: .normal)
        searchButton.setImage(UIImage(named: "Search_navbar_icon"), for: .highlighted)
        let backBarButtonItem = UIBarButtonItem.init(customView: searchButton)
        searchButton.addTarget(self, action: #selector(self.searchAction), for: .touchDown)
        navigationItem.rightBarButtonItem = backBarButtonItem
    }
    
    private func setupSortButton() {
        let width = sortButton.bounds.width
        sortButton.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -width - 40)
    }
    
    private func setupCollectionView() {
        collectionView.register(UINib.init(nibName: MPLListCell.cellNibName(), bundle: nil),
                                forCellWithReuseIdentifier: MPLListCell.cellIdentifier())
        
        collectionView.delegate = dataManager
        collectionView.dataSource = dataManager
    }

    
    // MARK: - MPLProductListViewInput
    
    func setupTitle(title: String) {
        DispatchQueue.main.async {
            self.title = title
        }
    }
    
    func setupSort(type: MPLProductListPresenterRequestType) {
        DispatchQueue.main.async {
            self.sortButton.setTitle("НОВИНКИ",for: .normal)
            self.sortButton.setImage(UIImage.init(named: "Arrow_down_icon"), for: .normal)
        }
    }
    
    func configure(list: APIResponse.ProductList, reset: Bool) {
        dataManager?.configureData(data: list, reset: reset)
        collectionView.reloadData()
        
        if let count: Int = output?.filtersCount() {
            let title = count > 0 ? "ФИЛЬТРЫ (\(count))" : "ФИЛЬТРЫ"
            filterButton.setTitle(title, for: .normal)
        }
    }
    
    
    // MARK: - Sort actions
    
    @IBAction func sortDidTapped(_ sender: Any) {
        let sort = UIAlertController.init(title: "Сортировка", message: nil, preferredStyle: .actionSheet)
        sort.addAction(UIAlertAction(title: "Популярные", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction!) in
            self.output?.sortDidTapped(type: .popular)
            self.sortButton.setTitle("ПОПУЛЯРНЫЕ",for: .normal)
            self.sortButton.setImage(UIImage.init(named: "Arrow_down_icon"), for: .normal)
        }))
        sort.addAction(UIAlertAction(title: "Новинки", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction!) in
            self.output?.sortDidTapped(type: .new)
            self.sortButton.setTitle("НОВИНКИ",for: .normal)
            self.sortButton.setImage(UIImage.init(named: "Arrow_down_icon"), for: .normal)
        }))
        sort.addAction(UIAlertAction(title: "По убыванию цены", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction!) in
            self.output?.sortDidTapped(type: .priceDesc)
            self.sortButton.setTitle("ПО ЦЕНЕ",for: .normal)
            self.sortButton.setImage(UIImage.init(named: "Arrow_down_icon"), for: .normal)
        }))
        sort.addAction(UIAlertAction(title: "По возрастанию цены", style: UIAlertAction.Style.default, handler: {
            (alert: UIAlertAction!) in
            self.output?.sortDidTapped(type: .priceAsc)
            self.sortButton.setTitle("ПО ЦЕНЕ",for: .normal)
            self.sortButton.setImage(UIImage.init(named: "Arrow_up_icon"), for: .normal)
        }))
        sort.addAction(UIAlertAction(title: "Отмена", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(sort, animated: true, completion: nil)
    }
    
    // MARK: - Action
    
    @objc private func searchAction() {
        output?.searchTapped()
    }
    
    @IBAction func filtersDidTapped(_ sender: Any) {
        output?.filtersDidTapped()
    }
}
