//
//  MPLProductListProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLProductListModuleConfigurator {
    func configureModule(id: String, listName: String, type: MPLProductListPresenterRequestType)
    func configureBrandsModule(variantID: String, featureID: String, listName: String)
}

protocol MPLProductListViewOutput {
    func viewDidLoad()
    func willDisplayLastProducts()
    func sortDidTapped(type: MPLProductListViewControllerSortType)
    func searchTapped()
    func filtersDidTapped()
    func filtersCount() -> Int
    func favouritesAdd(id: String)
}

protocol MPLProductListViewInput {
    func setupTitle(title: String)
    func setupSort(type: MPLProductListPresenterRequestType)
    func configure(list: APIResponse.ProductList, reset: Bool)
}

protocol MPLProductListInteractorOutput {
    func listWasDownloaded(list: APIResponse.ProductList)
    func favouritesAdded(added: Bool, message: String?)
    func needLoginFirst()
}

protocol MPLProductListInteractorInput {
    func configure(listType: MPLProductListPresenterRequestType)
    func downloadProducts(id: String)
    func downloadSpecialProducts(isSale: Bool)
    func downloadProductsByBrand(variantID: String, featureID: String)
    func downloadMore()
    func doSortedRequest(type: MPLProductListViewControllerSortType)
    func addFilersToRequest(filters: APIResponse.Filters)
    func selectedFiltersCount() -> Int
    func favouritesAdd(id: String)
}

protocol MPLProductListRouterInput {
    func showSearch()
    func showFilters(type: MPLFiltersInteractorRequestType, id: String, variantID: String, output: MPLFiltersModuleOutput) -> MPLFiltersViewController
    func showFilters(vc: MPLFiltersViewController)
    func showAuth()
}
