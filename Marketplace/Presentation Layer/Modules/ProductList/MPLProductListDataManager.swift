//
//  MPLProductListDataManager.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProductListDataManager: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MPLListCellOutput {

    // MARK: - Public vars & lets
    
    public var view: MPLProductListViewController?
    public var listEnded: Bool = false
    
    
    // MARK: - Private vars & lets
    
    private var data: APIResponse.ProductList?
    
    
    public func configureData(data: APIResponse.ProductList, reset: Bool) {
        if reset {
            self.data?.products.removeAll()
            listEnded = false
        }
        
        if data.products.count < 25 {
            listEnded = true
        }
        
        if self.data == nil  {
            self.data = data
            
        } else {
            self.data?.products += data.products
        }
    }
        
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.products.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MPLListCell.cellIdentifier(),
                                                      for: indexPath) as! MPLListCell
        let product = data!.products[indexPath.row]
        cell.configure(product: product)
        cell.output = self
        cell.layoutIfNeeded()
        MPLAppearance.addShadow(view: cell.frameView)
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row + 5 == (data?.products.count)! && !listEnded {
            view?.output?.willDisplayLastProducts()
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view?.collectionView.frame.size.width
        return CGSize(width: width! / 2 - 10, height: 275)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        return sectionInset
    }
    
    
    // MARK: - MPLListCellOutput
    
    func favourites(id: String) {
        view?.output?.favouritesAdd(id: id)
    }
    
    func productTapped(id: String, name: String) {
        
    }
}
