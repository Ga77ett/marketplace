//
//  MPLProductListPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 31/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

enum MPLProductListPresenterRequestType: Int {
    case list            = 0
    case popularProducts = 1
    case newestProducts  = 2
    case saleProducts    = 3
    case brands          = 4
    case search          = 5
}

class MPLProductListPresenter: NSObject, MPLProductListViewOutput, MPLProductListInteractorOutput, MPLProductListModuleConfigurator, MPLFiltersModuleOutput {
    

    // MARK: - Public vars & lets
    
    public var view: MPLProductListViewInput?
    public var interactor: MPLProductListInteractorInput?
    public var router: MPLProductListRouterInput?
    public var requestType: MPLProductListPresenterRequestType = .list
    
    
    // MARK: - Private vars & lets
    
    private var listID: String = ""
    private var listName: String = ""
    private var variantID: String = ""
    private var featureID: String = ""
    private var resetList: Bool = false
    private var filterView: MPLFiltersViewController?
    private var filteredProducts: APIResponse.ProductList?
    
    
    // MARK: - MPLProductListModuleConfigurator
    
    func configureModule(id: String, listName: String, type: MPLProductListPresenterRequestType) {
        self.listID = id
        self.listName = listName
        requestType = type
        interactor?.configure(listType: requestType)
        
        if type == .newestProducts {
            view?.setupSort(type: type)
        }
    }
    
    func configureBrandsModule(variantID: String, featureID: String, listName: String) {
        self.variantID = variantID
        self.featureID = featureID
        self.listName = listName
        requestType = .brands
        interactor?.configure(listType: requestType)
    }
    
    
    // MARK: - MPLProductListViewOutput
    
    func viewDidLoad() {
        view?.setupTitle(title: listName)
        doRequest()
    }
    
    func willDisplayLastProducts() {
        interactor?.downloadMore()
    }
    
    func sortDidTapped(type: MPLProductListViewControllerSortType) {
        resetList = true
        interactor?.doSortedRequest(type: type)
    }
    
    func searchTapped() {
        router?.showSearch()
    }
    
    func filtersDidTapped() {
        var type: MPLFiltersInteractorRequestType = .category
        var id = ""

        switch self.requestType {
        case .list:
            type = .category
            id = listID
        case .popularProducts:
            type = .category
            id = listID
        case .newestProducts:
            type = .newest
        case .saleProducts:
            type = .sale
        case .brands:
            type = .brands
            id = featureID
        case .search:
            type = .search
            id = listID
        }
        
        if filterView != nil {
            router?.showFilters(vc: (filterView as MPLFiltersViewController?)!)
            
        } else {
            filterView = router?.showFilters(type: type, id: id,
                                             variantID: variantID, output: self)
        }
    
        doRequest()
    }
    
    func filtersCount() -> Int {
        return interactor?.selectedFiltersCount() ?? 0
    }
    
    func favouritesAdd(id: String) {
        interactor?.favouritesAdd(id: id)
    }
    
    // MARK: - MPLProductListInteractorOutput
    
    func listWasDownloaded(list: APIResponse.ProductList) {
        if filterView != nil && filterView!.waitingCount {
            filterView?.showCount(productCount: list.params.totalItems)
            filteredProducts = list
            
        } else {
            view?.configure(list: list, reset: resetList)
            resetList = false
        }
    }
    
    func favouritesAdded(added: Bool, message: String?) {
        if !added {
            let errorView = MPLErrorMessage.instanceFromNib()
            errorView.show(message: message!)
        }
    }
    
    func needLoginFirst() {
        router?.showAuth()
    }
    
    
    // MARK: - MPLFiltersModuleOutput
    
    func checkProductCount(filters: APIResponse.Filters) {
        interactor?.addFilersToRequest(filters: filters)
    }
    
    func viewWillClose() {
        filterView = nil
        filteredProducts = nil
    }
    
    func filtersWasAccepted() {
        guard let list = filteredProducts else { return }
        view?.configure(list: list, reset: true)
        filteredProducts = nil
    }
    
    
    // MARK: - Private
    
    private func doRequest() {
        switch requestType {
        case .list, .popularProducts:
            interactor?.downloadProducts(id: listID)
        case .newestProducts:
            interactor?.downloadSpecialProducts(isSale: false)
        case .saleProducts:
            interactor?.downloadSpecialProducts(isSale: true)
        case .brands:
            interactor?.downloadProductsByBrand(variantID: variantID, featureID: featureID)
        case .search:
            interactor?.downloadProducts(id: listID)
        }
    }
}
