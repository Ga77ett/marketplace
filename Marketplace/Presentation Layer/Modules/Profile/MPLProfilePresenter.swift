//
//  MPLProfilePresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 06/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProfilePresenter: NSObject, MPLProfileViewOutput, MPLProfileInteractorOutput, MPLAuthModuleOutput {

    // MARK: - Public vars & lets
    
    public var view: MPLProfileViewInput?
    public var interactor: MPLProfileInteractorInput?
    public var router: MPLProfileRouter?
    
    
    // MARK: - Public vars & lets
    
    
    // MARK - MPLProfileViewOutput
    
    func viewDidLoad() {
        interactor?.downloadProfile()
    }
    
    func noToken() {
        router?.showAuth(output: self)
    }
    
    
    // MARK: - MPLProfileInteractorOutput
    
    func profileDownloaded(profile: APIResponse.Profile) {
        view?.configure(profile: profile)
    }
    
    
    // MARK: - MPLAuthModuleOutput
    
    func viewDidClose() {
        router?.showMain()
    }
    
    func tokenWasReceived() {
        view?.moveBlockView()
        interactor?.downloadProfile()
    }
}
