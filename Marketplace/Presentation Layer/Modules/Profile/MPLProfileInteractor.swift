//
//  MPLProfileInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 06/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProfileInteractor: MPLBaseInteractor, MPLProfileInteractorInput {

    // MARK: - Public vars & lets
    
    public var output: MPLProfileInteractorOutput?
    
    
    // MARK: - Private vars & lets
    
    
    // MARK: - MPLProfileInteractorInput
    
    func downloadProfile() {
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.getProfile,
                        params: ["client_token" : token], handler: {
            (res: Swift.Result<APIResponse.Profile, AlertMessage>) in
                switch res {
                case .success(let profile):
                    self.output?.profileDownloaded(profile: profile)
                    break
                case .failure(let message):
                    print(message.body)
                    break
            }
        })
    }
}
