//
//  MPLProfileViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 06/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProfileViewController: UITableViewController, MPLProfileViewInput, MPLPersonalDataViewControllerOutput, UIAdaptivePresentationControllerDelegate {

    // MARK: - Public vars & lets
    
    public var output: MPLProfileViewOutput?
    @IBOutlet weak var placeholderImageView: UIImageView!
    @IBOutlet weak var anagramFrameView: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var anagramLabel: UILabel!
    @IBOutlet weak var placeholderHeightConstraint: NSLayoutConstraint!
    
    
    // MARK: - Private vars & lets
    
    private let blockView: UIView = UIView.init()
    private var profile: APIResponse.Profile?
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLProfileAssembly.createModule(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        output?.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        moveBlockView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let header = tableView.tableHeaderView else { return }
        let offsetY = -tableView.contentOffset.y
        placeholderHeightConstraint.constant = max(header.bounds.height, header.bounds.height + offsetY)
    }

    private func setupInitialState() {
        setupTableView()
        setupHeader()
        moveBlockView()
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
    }
    
    private func setupHeader() {
        anagramFrameView.layer.cornerRadius = anagramFrameView.bounds.size.width / 2
        anagramFrameView.clipsToBounds = true
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
            showAlert()
        }
    }
    
    
    // MARK: - MPLProfileViewInput
    
    func configure(profile: APIResponse.Profile) {
        self.profile = profile
        DispatchQueue.main.async {
            self.emailLabel.text = profile.data.email
            self.nameLabel.text = "\(profile.data.firstName ?? "") \(profile.data.lastName ?? "")"
            guard let firstName = profile.data.firstName else { return }
            guard let lastName = profile.data.firstName else { return }
            self.anagramLabel.text = "\(firstName.first!)\(lastName.first!)".uppercased()
        }
    }
    
    func moveBlockView() {
        if UserDefaults.standard.object(forKey: kMPLUserToken) == nil {
            output?.noToken()
            blockView.frame = CGRect.init(x: 0, y: -44,
                                          width: self.view.frame.size.width,
                                          height: self.view.frame.size.height)
            blockView.backgroundColor = UIColor.white
            self.view.addSubview(blockView)
            
        } else {
            blockView.removeFromSuperview()
        }
    }
    
    
    // MARK: - Helper
    
    private func showAlert() {
        let alert = UIAlertController(title: nil,
                                      message: "Вы действительно хотите выйти?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Да",
                                      style: UIAlertAction.Style.default, handler: { action in
                                    self.exitAction()
        }))
        
        alert.addAction(UIAlertAction(title: "Отмена",
                                      style: UIAlertAction.Style.cancel, handler:nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func exitAction() {
        UserDefaults.standard.set(nil, forKey: kMPLUserToken)
        UserDefaults.standard.synchronize()
        navigationController?.tabBarController?.selectedIndex = 0
    }
    
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MPLPersonalDataViewControllerSegue"{
            if let nextViewController = segue.destination as? MPLPersonalDataViewController {
                guard let data = profile else { return }
                nextViewController.configure(profile: data, output: self)
            }
        }
        
        if segue.identifier == "MPLMyAddressViewControllerSegue"{
            if let nextViewController = segue.destination as? MPLMyAddressViewController {
                guard let data = profile else { return }
                nextViewController.configure(profile: data, output: self)
            }
        }
    }
    
    
    // MARK: - MPLPersonalDataViewControllerOutput
    
    func dataChanged() {
        output?.viewDidLoad()
    }
}
