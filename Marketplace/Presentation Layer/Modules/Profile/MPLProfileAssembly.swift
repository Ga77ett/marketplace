//
//  MPLProfileAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 06/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProfileAssembly: NSObject {
    class func createModule(viewController: MPLProfileViewController) {
        let presenter = MPLProfilePresenter()
        let interactor = MPLProfileInteractor()
        let router = MPLProfileRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
