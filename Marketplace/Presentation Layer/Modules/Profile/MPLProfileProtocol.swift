//
//  MPLProfileProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 06/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLProfileViewOutput {
    func viewDidLoad()
    func noToken()
}

protocol MPLProfileViewInput {
    func configure(profile: APIResponse.Profile)
    func moveBlockView()
}

protocol MPLProfileInteractorOutput {
    func profileDownloaded(profile: APIResponse.Profile)
}

protocol MPLProfileInteractorInput {
    func downloadProfile()
}

protocol MPLProfileRouterInput {
    func showAuth(output: MPLAuthModuleOutput)
    func showMain()
}
