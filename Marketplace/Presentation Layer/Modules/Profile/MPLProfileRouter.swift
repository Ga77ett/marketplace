//
//  MPLProfileRouter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 06/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProfileRouter: MPLBaseRouter, MPLProfileRouterInput {

    // MARK: - MPLProfileRouterInput
    
    func showAuth(output: MPLAuthModuleOutput) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Authorization", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLAuthViewController") as! MPLAuthViewController
            let presenter = controller.output as! MPLAuthPresenter
            presenter.configure(output: output)
            let navVC = UINavigationController.init(rootViewController: controller)
            self.view?.navigationController?.present(navVC, animated: false, completion: nil)
        }
    }
    
    func showMain() {
        DispatchQueue.main.async {
            self.view?.navigationController?.tabBarController?.selectedIndex = 0
        }
    }
}
