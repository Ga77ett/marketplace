//
//  MPLSearchAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 09/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSearchAssembly: NSObject {
    class func createModule(viewController: MPLSearchViewController) {
        let presenter = MPLSearchPresenter()
        let interactor = MPLSearchInteractor()
        let router = MPLSearchRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
