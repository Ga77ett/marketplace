//
//  MPLSearchViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 09/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSearchViewController: UITableViewController, MPLSearchViewInput, UISearchBarDelegate, UISearchControllerDelegate {
    
    // MARK: - Public vars & lets
    
    public var output: MPLSearchViewOutput?
    
    
    // MARK: - Private vars & lets
    
    private var data: [String] = []
    
    //private var dataSource: [ ]
    private let searchController = UISearchController(searchResultsController: nil)

    
    // MARK: - Setup
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLSearchAssembly.createModule(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        guard let data = UserDefaults.standard.object(forKey: "kMPLSearchHistoryKey") as? [String] else { return }
        self.data = data
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.searchController.searchBar.becomeFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        definesPresentationContext = true
        setupInitialState()
        output?.viewDidLoad()
    }
    
    private func setupInitialState() {
        setupTableView()
        setupSearchBar()
        setupToolbar()
    }

    private func setupTableView() {
        tableView.register(UINib(nibName: MPLTextCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLTextCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }

    func setupSearchBar() {
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.delegate = self
        searchController.delegate = self
        
        searchController.searchBar.placeholder = "Поиск товаров"
        searchController.searchBar.setImage(UIImage.init(named: ""), for: .bookmark, state: .normal)
        searchController.searchBar.searchBarStyle = .minimal
        //searchController.searchBar.sizeToFit()
        searchController.searchBar.barTintColor = navigationController?.navigationBar.barTintColor
        searchController.searchBar.tintColor = self.view.tintColor
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Отменить"
        let attributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.foregroundColor : UIColor.darkBlueGrey,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .semibold)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        
        navigationItem.titleView = searchController.searchBar
    }
    
    func setupToolbar() {
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDidTapped))]
        toolbar.sizeToFit()
        searchController.searchBar.inputAccessoryView = toolbar
    }
    
    @objc func doneDidTapped() {
        searchController.searchBar.resignFirstResponder()
    }
    
    
    // MARK: - UISearchBarDelegate
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationController?.popViewController(animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        output?.search(text: text)
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count == 0 {
            return 0
            
        } else {
            return data.count + 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLTextCell.cellIdentifier(), for: indexPath) as! MPLTextCell
        
        if indexPath.row >= data.count {
            cell.configureDestructive()
            
        } else {
            cell.configure(text: data[indexPath.row])
        }
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLTextCell.cellHeight()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= data.count {
            UserDefaults.standard.removeObject(forKey: "kMPLSearchHistoryKey")
            data.removeAll()
            tableView.reloadData()
            
        } else {
            output?.search(text: data[indexPath.row])
        }
    }
}
