//
//  MPLSearchRouter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 09/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSearchRouter: MPLBaseRouter, MPLSearchRouterInput {

    // MARK: - MPLSearchRouterInput
    
    func showProducts(text: String) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLProductListViewController") as! MPLProductListViewController
            let presenter = controller.output as! MPLProductListPresenter
            presenter.configureModule(id: text, listName: text, type: .search)
            
            self.view?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
