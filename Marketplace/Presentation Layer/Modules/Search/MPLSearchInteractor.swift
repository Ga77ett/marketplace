//
//  MPLSearchInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 09/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSearchInteractor: MPLBaseInteractor, MPLSearchInteractorInput {

    // MARK: - Public vars & lets
    
    public var output: MPLSearchInteractorOutput?
}
