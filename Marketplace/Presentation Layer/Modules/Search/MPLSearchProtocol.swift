//
//  MPLSearchProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 09/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLSearchViewOutput {
    func viewDidLoad()
    func search(text: String)
}

protocol MPLSearchViewInput {

}

protocol MPLSearchInteractorOutput {
    
}

protocol MPLSearchInteractorInput {
    
}

protocol MPLSearchRouterInput {
    func showProducts(text: String)
}
