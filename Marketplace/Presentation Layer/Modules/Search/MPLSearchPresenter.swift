//
//  MPLSearchPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 09/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSearchPresenter: NSObject, MPLSearchViewOutput, MPLSearchInteractorOutput {

    // MARK: Public vars & lets
    
    public var view: MPLSearchViewInput?
    public var interactor: MPLSearchInteractorInput?
    public var router: MPLSearchRouterInput?
    
    
    // MARK: - MPLSearchViewOutput
    
    func viewDidLoad() {

    }
    
    func search(text: String) {
        router?.showProducts(text: text)
    }
}
