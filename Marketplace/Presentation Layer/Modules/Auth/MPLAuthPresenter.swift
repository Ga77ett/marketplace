//
//  MPLAuthPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLAuthPresenter: NSObject, MPLAuthViewOutput, MPLAuthInteractorOutput, MPLAuthModuleInput {

    // MARK: - Public vars & lets
    
    public var view: MPLAuthViewInput?
    public var interactor: MPLAuthInteractorInput?
    public var router: MPLAuthRouter?
    public var output: MPLAuthModuleOutput?
    
    
    // MARK: - Public vars & lets
    
    
    // MARK: - MPLAuthModuleInput
    
    func configure(output: MPLAuthModuleOutput?) {
        self.output = output
    }
    
    
    // MARK - MPLAuthViewOutput
    
    func viewDidLoad() {
        
    }
    
    func fieldsFilled(email: String, pass: String) {
        interactor?.auth(email: email, pass: pass)
    }
    
    func viewDidClose() {
        output?.viewDidClose()
    }
    
    
    // MARK: - MPLAuthInteractorOutput
    
    func authCompleted(success: Bool, errorMessage: String?) {
        view?.authorization(success: success, message: errorMessage)
        output?.tokenWasReceived()
    }
}
