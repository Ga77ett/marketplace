//
//  MPLAuthAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLAuthAssembly: NSObject {
    class func createModule(viewController: MPLAuthViewController) {
        let presenter = MPLAuthPresenter()
        let interactor = MPLAuthInteractor()
        let router = MPLAuthRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
