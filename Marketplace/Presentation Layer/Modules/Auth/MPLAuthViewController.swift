//
//  MPLAuthViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MPLAuthViewController: UIViewController, MPLAuthViewInput {
    
    // MARK: - Public vars & lets
    
    public var output: MPLAuthViewOutput?
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passErrorLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    
    // MARK: - Private vars & lets
    
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLAuthAssembly.createModule(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        setupInitialState()
        output?.viewDidLoad()
    }
    
    private func setupInitialState() {
        setupCloseButton()
    }
    
    private func setupCloseButton() {
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x:0, y:0, width: 24, height:24)
        closeButton.setImage(UIImage(named: "Close_icon"), for: .normal)
        closeButton.setImage(UIImage(named: "Close_icon"), for: .highlighted)
        let leftButtonItem = UIBarButtonItem.init(customView: closeButton)
        closeButton.addTarget(self, action: #selector(self.closeAction), for: .touchDown)
        navigationItem.leftBarButtonItem = leftButtonItem
    }
    
    
    // MARK: - Actions
    
    @IBAction func doneAction(_ sender: Any) {
        guard let email = emailField.text else { return }
        var emailIsValid = false
        if email.isEmpty || !MPLValidation.validate(email: email) {
            emailErrorLabel.isHidden = false
            
        } else {
            emailErrorLabel.isHidden = true
            emailIsValid = true
        }
        emailField.toogleErrorState(error: !emailIsValid)
        
        guard let pass = passField.text else { return }
        var passIsValid = false
        if pass.isEmpty {
            passErrorLabel.isHidden = false
            
        } else {
            passErrorLabel.isHidden = true
            passIsValid = true
        }
        passField.toogleErrorState(error: !passIsValid)
        
        if emailIsValid && passIsValid {
            output?.fieldsFilled(email: email, pass: pass)
        }
    }
    
    @IBAction func secureAction(_ sender: Any) {
        passField.isSecureTextEntry.toggle()
    }
    
    @objc func closeAction() {
        navigationController?.dismiss(animated: true, completion: nil)
        output?.viewDidClose()
    }
    
    
    // MARK: - MPLAuthViewInput
    
    func authorization(success: Bool, message: String?) {
        if success {
            navigationController?.dismiss(animated: true, completion: nil)
            
        } else {
            let errorView = MPLErrorMessage.instanceFromNib()
            errorView.show(message: message!)
        }
    }
}
