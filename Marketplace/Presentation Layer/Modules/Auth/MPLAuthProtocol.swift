//
//  MPLAuthProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLAuthModuleOutput {
    func viewDidClose()
    func tokenWasReceived()
}

protocol MPLAuthModuleInput {
    func configure(output: MPLAuthModuleOutput?)
}

protocol MPLAuthViewOutput {
    func viewDidLoad()
    func fieldsFilled(email: String, pass: String)
    func viewDidClose()
}

protocol MPLAuthViewInput {
    func authorization(success: Bool, message: String?)
}

protocol MPLAuthInteractorOutput {
    func authCompleted(success: Bool, errorMessage: String?)
}

protocol MPLAuthInteractorInput {
    func auth(email: String, pass: String)
}

protocol MPLAuthRouterInput {
}
