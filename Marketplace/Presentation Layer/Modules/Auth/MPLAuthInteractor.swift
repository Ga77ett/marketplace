//
//  MPLAuthInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLAuthInteractor: MPLBaseInteractor, MPLAuthInteractorInput {

    // MARK: - Public vars & lets
    
    public var output: MPLAuthInteractorOutput?
    
    
    // MARK: - Private vars & lets
    
    
    
    // MARK: - MPLAuthInteractorInput
    
    func auth(email: String, pass: String) {
        apiManager.call(type: RequestItemsType.auth, params: ["email": email, "password": pass], handler: {
            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
            switch res {
            case .success(let response):
                let success = response.errorCode == 0 ? true: false
                if success {
                    UserDefaults.standard.set(response.token!, forKey: kMPLUserToken)
                    UserDefaults.standard.synchronize()
                }
                
                self.output?.authCompleted(success: success, errorMessage: response.errorMessage)
                
                break
            case .failure(let message):
                print(message.body)
                break
            }
        })
    }
}
