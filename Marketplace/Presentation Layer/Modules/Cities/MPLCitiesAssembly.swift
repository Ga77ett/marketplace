//
//  MPLCitiesAssembly.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 29/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCitiesAssembly: NSObject {
    class func createModule(viewController: MPLCitiesViewController) {
        let presenter = MPLCitiesPresenter()
        let interactor = MPLCitiesInteractor()
        let router = MPLCitiesRouter()
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter
        router.view = viewController
    }
}
