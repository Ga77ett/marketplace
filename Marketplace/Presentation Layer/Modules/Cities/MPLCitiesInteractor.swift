//
//  MPLCitiesInteractor.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 29/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCitiesInteractor: MPLBaseInteractor, MPLCitiesInteractorInput {

    // MARK: - Public vars & lets
    
    public var output: MPLCitiesInteractorOutput?
    
    
    // MARK: - Private vars & lets
    
    private var page: Int           = 1
    private let itemsPerPage        = 50
    private var selectedCityID: String?
    private var searchString: String?
    
    
    // MARK: - MPLCitiesInteractorInput
    
    func downloadCities(selectedCityID: String?) {
        self.selectedCityID = selectedCityID
        var params: [String: Any] = ["items_per_page" : itemsPerPage, "page" : page]
        if searchString != nil { params["q"] = searchString }
        apiManager.call(type: RequestItemsType.cities,
                        params: params) {
            (res: Swift.Result<APIResponse.Cities, AlertMessage>) in
            switch res {
            case .success(let cities):
                self.markSelectedCity(list: cities)
                break
            case .failure(let message):
                print(message.body)
                break
            }
        }
    }
    
    func downloadMore() {
        page += 1
        downloadCities(selectedCityID: selectedCityID)
    }
    
    func searchCity(name: String) {
        page = 1
        searchString = name
        downloadCities(selectedCityID: selectedCityID)
    }
    
    
    // MARK: - Helper
    
    private func markSelectedCity(list: APIResponse.Cities) {
        var mutableList = list
        
        if selectedCityID != nil {
            if let row = mutableList.list.firstIndex(where: {$0.id == selectedCityID}) {
                mutableList.list[row].selected = true
            }
        }
        
        output?.citiesDownloaded(cities: mutableList)
    }
}
