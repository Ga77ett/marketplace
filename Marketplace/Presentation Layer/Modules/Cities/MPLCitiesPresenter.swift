//
//  MPLCitiesPresenter.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 29/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCitiesPresenter: NSObject, MPLCitiesViewOutput, MPLCitiesInteractorOutput,
MPLCitiesModuleConfiguration {

    // MARK: Public vars & lets
    
    public var view: MPLCitiesViewInput?
    public var interactor: MPLCitiesInteractorInput?
    public var router: MPLCitiesRouter?
    
    
    // MARK: Public vars & lets
    
    private var output: MPLCitiesModuleOutput?
    private var selectedCityID: String?
    
    
    // MARK: - MPLCitiesModuleConfiguration
    
    func configure(cityID: String?, output: MPLCitiesModuleOutput) {
        self.output = output
        selectedCityID = cityID
    }
    
    
    // MARK: - MPLCitiesViewOutput
    
    func viewDidLoad() {
        interactor?.downloadCities(selectedCityID: selectedCityID)
    }
    
    func willDisplayLastCities() {
        interactor?.downloadMore()
    }
    
    func citySelected(cityID: String, name: String) {
        output?.citySelected(cityID: cityID, name: name)
    }
    
    func searchCity(name: String) {
        interactor?.searchCity(name: name)
    }
    
    
    // MARK: - MPLInteractorOutput
    
    func citiesDownloaded(cities: APIResponse.Cities) {
        view?.configure(cities: cities)
    }
}
