//
//  MPLCitiesProtocol.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 29/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

protocol MPLCitiesModuleOutput {
    func citySelected(cityID: String, name: String)
}

protocol MPLCitiesModuleConfiguration {
    func configure(cityID: String?, output: MPLCitiesModuleOutput)
}

protocol MPLCitiesViewOutput {
    func viewDidLoad()
    func willDisplayLastCities()
    func citySelected(cityID: String, name: String)
    func searchCity(name: String)
}

protocol MPLCitiesViewInput {
    func configure(cities: APIResponse.Cities)
}

protocol MPLCitiesInteractorOutput {
    func citiesDownloaded(cities: APIResponse.Cities)
}

protocol MPLCitiesInteractorInput {
    func downloadCities(selectedCityID: String?)
    func downloadMore()
    func searchCity(name: String)
}

protocol MPLCitiesRouterInput {

}
