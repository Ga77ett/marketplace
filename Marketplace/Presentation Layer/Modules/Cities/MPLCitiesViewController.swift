//
//  MPLCitiesViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 29/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCitiesViewController: UIViewController, MPLCitiesViewInput, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - Public vars & lets
    
    public var output: MPLCitiesViewOutput?
    public var data: [APIResponse.Cities.City] = []
    public var initialData: [APIResponse.Cities.City] = []
    public var listEnded: Bool = false
    public var searchIsActive: Bool = false
    
    
    // MARK: - Private vars & lets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        MPLCitiesAssembly.createModule(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        setupInitialState()
        output?.viewDidLoad()
    }
    
    private func setupInitialState() {
        setupTableView()
        setupSearchBar()
        setupToolbar()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLSearchCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLSearchCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
        
        searchBar.placeholder = "Поиск"
        searchBar.setImage(UIImage.init(named: ""), for: .bookmark, state: .normal)
        searchBar.searchBarStyle = .minimal
        searchBar.barTintColor = navigationController?.navigationBar.barTintColor
        searchBar.tintColor = self.view.tintColor
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Отменить"
        let attributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.foregroundColor : UIColor.darkBlueGrey,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .semibold)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }
    
    func setupToolbar() {
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
                         UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDidTapped))]
        toolbar.sizeToFit()
        searchBar.inputAccessoryView = toolbar
    }
    
    @objc func doneDidTapped() {
        searchBar.resignFirstResponder()
        searchIsActive = false
    }
    

    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLSearchCell.cellIdentifier(), for: indexPath) as! MPLSearchCell
        cell.configure(city: data[indexPath.row])
        return cell
    }

    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLSearchCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if data.count > 0 && indexPath.row + 5 == data.count && !listEnded {
            output?.willDisplayLastCities()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output?.citySelected(cityID: data[indexPath.row].id, name: data[indexPath.row].name)
        navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - MPLCitiesViewInput
    
    func configure(cities: APIResponse.Cities) {
        if cities.list.count < 50 {
            listEnded = true
        }
        
        data += cities.list
        if !searchIsActive {
            initialData += cities.list
        }
        
        tableView.reloadData()
    }
    
    
    // MARK: - UISearchBarDelegate
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        
        self.data = initialData
        searchIsActive = false
        tableView.reloadData()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else { return }
        
        if (text.count > 0) {
            searchIsActive = true
            data.removeAll()
            output?.searchCity(name: text)
            
        } else {
            self.data.removeAll()
            data = initialData
        }
        
        tableView.reloadData()
    }
}
