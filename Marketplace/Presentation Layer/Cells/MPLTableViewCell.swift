//
//  MPLTableViewCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 19/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLTableViewCell: UITableViewCell {
    
    class func cellHeight() -> CGFloat {
        return 0
    }
    
    class func cellNibName() -> String {
        return cellIdentifier()
    }
    
    class func cellIdentifier() -> String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
