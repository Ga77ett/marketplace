//
//  MPLImageCarouselCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 25/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SDWebImage

class MPLImageCarouselCell: MPLTableViewCell, MPLImageCarouselCollectionViewOutput {

    // MARK: - Public vars & lets
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var collectionView: MPLImageCarouselCollectionView!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    // MARK: - Private vars & lets
    
    private var data: [APIResponse.Product.Image]?
    private var selectedIndex: Int = 0
    
    
    // MARK: - Init
    
    override class func cellHeight() -> CGFloat {
        return 370
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.output = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func configure(images: [APIResponse.Product.Image], disc: String?) {
        if disc != nil {
            badgeImageView.isHidden = false
            discountLabel.isHidden = false
            discountLabel.text = ("\(disc!)%")
        }
        
        if images.count == 1 {
            collectionView.isHidden = true
            heightConstraint.constant = 0
            
        } else {
            collectionView.configure(images: images)
            data = images
            
            let swipeLeft = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeToLeft))
            swipeLeft.direction = .left
            self.addGestureRecognizer(swipeLeft)
            
            let swipeRight = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeToRight))
            swipeRight.direction = .right
            self.addGestureRecognizer(swipeRight)
        }
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        productImageView!.sd_setImage(with: URL(string: images[0].largeURL), placeholderImage: nil,
                                      options: [.continueInBackground, .highPriority], context: nil)
    }
    
    func reloadImage() {
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        productImageView!.sd_setImage(with: URL(string: data![selectedIndex].largeURL), placeholderImage: nil,
                                      options: [.continueInBackground, .highPriority], context: nil)
    }
    
    
    // MARK: - MPLImageCarouselCollectionViewOutput
    
    func imageSelected(index: Int) {
        selectedIndex = index
        reloadImage()
    }
    
    
    // MARK: Gesture
    
    @objc func swipeToLeft() {
        let newIndex = selectedIndex + 1
        let imagesCount = data?.count ?? 0
        if newIndex < imagesCount {
            selectedIndex = newIndex
            
        } else {
            selectedIndex = 0
        }
        
        reloadImage()
        collectionView.reloadIndex(index: selectedIndex)
    }
    
    @objc func swipeToRight() {
        let newIndex = selectedIndex - 1
        let imagesCount = data?.count ?? 1
        if newIndex >= 0 {
            selectedIndex = newIndex
            
        } else {
            selectedIndex = imagesCount - 1
        }
        
        reloadImage()
        collectionView.reloadIndex(index: selectedIndex)
    }
}
