//
//  MPLImageCarouselCollectionView.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 26/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

protocol MPLImageCarouselCollectionViewOutput {
    func imageSelected(index: Int)
}

class MPLImageCarouselCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {

    // MARK: - Public vars & lets
    
    public var output: MPLImageCarouselCollectionViewOutput?
    
    
    // MARK: - Private vars & lets
    
    private var data: [APIResponse.Product.Image]?
    private var selectedIndex: Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.delegate = self
        self.dataSource = self
        self.register(UINib.init(nibName: MPLSmallImageCell.cellNibName(), bundle: nil),
                      forCellWithReuseIdentifier: MPLSmallImageCell.cellIdentifier())
    }
    
    public func configure(images: [APIResponse.Product.Image]) {
        data = images
        self.reloadData()
    }
    
    public func reloadIndex(index: Int) {
        selectedIndex = index
        self.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MPLSmallImageCell.cellIdentifier(),
                                                      for: indexPath) as! MPLSmallImageCell
        let image = data![indexPath.row]
        cell.configure(image: image, selected: selectedIndex == indexPath.row ? true: false)
        return cell
    }
    
    
    // MARK: - UICollectionDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        output?.imageSelected(index: selectedIndex)
        self.reloadData()
    }
}
