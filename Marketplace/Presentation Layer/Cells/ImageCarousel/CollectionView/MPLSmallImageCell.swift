//
//  MPLSmallImageCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 26/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SDWebImage

class MPLSmallImageCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var alphaView: UIView!
    
    class func cellHeight() -> CGFloat {
        return 60
    }
    
    class func cellNibName() -> String {
        return cellIdentifier()
    }
    
    class func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    func configure(image: APIResponse.Product.Image, selected: Bool) {
        if selected {
            alphaView.isHidden = true
        }
        
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        productImageView!.sd_setImage(with: URL(string: image.smallURL), placeholderImage: nil,
                                      options: [.continueInBackground, .highPriority], context: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        alphaView.isHidden = false
    }
}
