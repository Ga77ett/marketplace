//
//  MPLTopCategoryCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 07/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLTopCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    class func cellHeight() -> CGFloat {
        return 120
    }
    
    class func cellNibName() -> String {
        return cellIdentifier()
    }
    
    class func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.cornerRadius = 10
        frameView.clipsToBounds = true
    }
    
    func configure(category: APIResponse.Category) {
        guard let mainPair = category.mainPair else { return }
        categoryImageView.sd_setImage(with: URL.init(string: mainPair.detailed.imagePath), completed: nil)
        nameLabel.text = category.name
        
        MPLAppearance.addShadow(view: frameView)
    }
}
