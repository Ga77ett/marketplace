//
//  MPLCategoriesCollectionView.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 01/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLCategoriesCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {

    private var data: APIResponse.CategoriesList?
    private var output: MPLTopCategoriesCellOutput?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.register(UINib.init(nibName: MPLTopCategoryCell.cellNibName(), bundle: nil),
                      forCellWithReuseIdentifier: MPLTopCategoryCell.cellIdentifier())
    }
    
    public func configure(categories: APIResponse.CategoriesList, output: MPLTopCategoriesCellOutput) {
        data = categories
        self.output = output
    }
    
    class func cellSize() -> CGSize {
        let cellSpacing = CGFloat(0) //Define the space between each cell
        let leftRightMargin = CGFloat(10) //If defined in Interface Builder for "Section Insets"
        let numColumns = CGFloat(3) //The total number of columns you want
        
        let totalCellSpace = cellSpacing * (numColumns - 1)
        let screenWidth = UIScreen.main.bounds.width
        let width = (screenWidth - leftRightMargin - totalCellSpace) / numColumns
        let height = CGFloat(width) //whatever height you want
        
        return CGSize(width: width, height: height) // width & height are the same to make a square cell
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.categories.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MPLTopCategoryCell.cellIdentifier(),
                                                      for: indexPath) as! MPLTopCategoryCell
        let category = data!.categories[indexPath.row]
        cell.configure(category: category)
        return cell
    }
    
    
    // MARK: - UICollectionDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = data!.categories[indexPath.row]
        output?.popularCategoryTapped(id: category.categoryID, name: category.name)
    }

}

extension MPLCategoriesCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 3 - 12
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        return sectionInset
    }
}
