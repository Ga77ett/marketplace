//
//  MPLTopCategoriesCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 01/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

protocol MPLTopCategoriesCellOutput {
    func popularCategoryTapped(id: String, name: String)
}

class MPLTopCategoriesCell: MPLTableViewCell {

    @IBOutlet weak var collectionView: MPLCategoriesCollectionView!
    
    override class func cellHeight() -> CGFloat {
        let size = MPLCategoriesCollectionView.cellSize()
        return size.height * 2
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = collectionView
        collectionView.delegate = collectionView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    public func configure(categories: APIResponse.CategoriesList, output: MPLTopCategoriesCellOutput) {
        collectionView.configure(categories: categories, output: output)
        collectionView.reloadData()
    }
}
