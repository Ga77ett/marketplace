//
//  MPLIncomingCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 18/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLIncomingCell: MPLTableViewCell {

    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.cornerRadius = 10
        frameView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func configure(thread: APIResponse.Chat.Message) {
        messageLabel.text = thread.message
        
        if let time = (Double(thread.timestamp)) {
            let date = Date(timeIntervalSince1970: time)
            let calendar = Calendar.current
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            dateLabel.text = dateFormatter.string(from: date)
            
            if calendar.isDateInYesterday(date) {
                dateLabel.text = "Вчера, \(dateFormatter.string(from: date))"
                
            } else if calendar.isDateInToday(date) {
                dateLabel.text = "Сегодня, \(dateFormatter.string(from: date))"
                
            } else {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMMM, HH:mm"
                dateLabel.text = dateFormatter.string(from: date)
            }
        }
    }
}
