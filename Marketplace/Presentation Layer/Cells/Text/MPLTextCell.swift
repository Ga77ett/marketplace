//
//  MPLTextCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 11/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLTextCell: MPLTableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var clearLabel: UILabel!
    
    
    override class func cellHeight() -> CGFloat {
        return 60
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(text: String) {
        label.text = text
        
        let imageView: UIImageView = UIImageView(frame:CGRect(x: 0, y: 0, width: 13, height: 13))
        imageView.image = UIImage(named:"Accessory_icon")
        imageView.contentMode = .scaleAspectFit
        accessoryView = imageView
    }
    
    func configureDestructive() {
        clearLabel.isHidden = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clearLabel.isHidden = true
        label.text = ""
    }
}
