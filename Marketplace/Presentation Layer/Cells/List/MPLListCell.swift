//
//  MPLListCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 01/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SDWebImage

protocol MPLListCellOutput {
    func favourites(id: String)
    func productTapped(id: String, name: String)
}

class MPLListCell: UICollectionViewCell {
    
    // MARK: - Public vars & lets
    
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var favouritesButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    public var output: MPLListCellOutput?
    
    
    // MARK: - Private vars & lets
    
    private var id: String = ""
    private var inWishlist: Bool = false
    
    
    class func cellHeight() -> CGFloat {
        return 275
    }
    
    class func cellNibName() -> String {
        return cellIdentifier()
    }
    
    class func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.cornerRadius = 10
        frameView.clipsToBounds = true
        
        cartButton.layer.cornerRadius = 10
        cartButton.clipsToBounds = true
    }
    
    func configure(product: APIResponse.Product) {
        descLabel.text = product.name
        id = product.id
        inWishlist = product.inWishlist!
        
        if product.subtype == "advert" {
            cartButton.isHidden = true
            dateLabel.isHidden = false
            
            if let time = (Double(product.timestamp)) {
                let date = Date(timeIntervalSince1970: time)
                let calendar = Calendar.current
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                dateLabel.text = dateFormatter.string(from: date)
                
                if calendar.isDateInYesterday(date) {
                    dateLabel.text = "Вчера, \(dateFormatter.string(from: date))"
                    
                } else if calendar.isDateInToday(date) {
                    dateLabel.text = "Сегодня, \(dateFormatter.string(from: date))"
                    
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd MMMM, HH:mm"
                    dateLabel.text = dateFormatter.string(from: date)
                }
            }
        }
        
        let floatPrice = Float(product.price) 
        let floatOldPrice = Float(product.oldPrice) 
        
        if floatOldPrice > Float(0) {
            let priceStr = String(format: "%.0f", floatPrice)
            let listPriceStr = String(format: "%.0f", floatOldPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr) ₽  \(listPriceStr) ₽", attributes: [
                .font: UIFont.systemFont(ofSize: 13.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08,
                ])
            attributedString.addAttributes([
                .font: UIFont.systemFont(ofSize: 15.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 250/255, green: 114/255, blue: 104/255, alpha: 1),
                ], range: NSRange(location: 0, length: listPriceStr.count + 2))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1,
                                          range: NSMakeRange(priceStr.count + 4, listPriceStr.count + 2))
            
            priceLabel.attributedText = attributedString
            
        } else {
            let priceStr = floatPrice == 0 ? "Бесплатно" : String(format: "%.0f ₽", floatPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr)", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08
                ])
            
            priceLabel.attributedText = attributedString
        }
        
        if product.discount != nil {
            badgeImageView.isHidden = false
            discountLabel.isHidden = false
            
            discountLabel.text = "\(product.discount!) %"
        }
        
        if product.inWishlist! {
            favouritesButton.setImage(UIImage.init(named: "Favourites_active"), for: .normal)
            
        } else {
            favouritesButton.setImage(UIImage.init(named: "Favourites_inactive"), for: .normal)
        }
        
        guard let imagePath = product.imagePath else { return }
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        SDWebImageDownloader.shared.setValue("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                                             forHTTPHeaderField: "Accept")
        productImageView!.sd_setImage(with: URL(string: imagePath), placeholderImage: nil,
                                      options: [.continueInBackground, .highPriority], context: nil)
    }
    
    @IBAction func favouritesAction(_ sender: Any) {
        output?.favourites(id: id)
        
        inWishlist = !inWishlist
        if inWishlist {
            favouritesButton.setImage(UIImage.init(named: "Favourites_active"), for: .normal)
            
        } else {
            favouritesButton.setImage(UIImage.init(named: "Favourites_inactive"), for: .normal)
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        badgeImageView.isHidden = true
        discountLabel.isHidden = true
        productImageView.af_cancelImageRequest()
        productImageView.image = UIImage(named: "")
        cartButton.isHidden = false
        dateLabel.isHidden = true
    }
}
