//
//  MPLFavouritesCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 11/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SDWebImage

protocol MPLFavouritesCellOutput {
    func delete(id: String)
}

class MPLFavouritesCell: MPLTableViewCell {
    
    // MARK: - Public vars & lets
    
    public var output: MPLFavouritesCellOutput?
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var cartButton: MPLButton!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var trashIcon: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    // MARK: - Private vars & lets
    
    private var id: String = ""
    
    
    override class func cellHeight() -> CGFloat {
        return 145
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.cornerRadius = 10
        frameView.clipsToBounds = true
        
        cartButton.layer.cornerRadius = 10
        cartButton.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(product: APIResponse.Product) {
        id = product.id
        nameLabel.text = product.name
        
        let floatPrice = Float(product.price)
        let floatOldPrice = Float(product.oldPrice)
        setupLabels(floatOldPrice: floatOldPrice, floatPrice: floatPrice)
        
        if product.discount != nil {
            badgeImageView.isHidden = false
            discountLabel.isHidden = false
            
            discountLabel.text = "\(product.discount!) %"
        }
        
        guard let imagePath = product.imagePath else { return }
        downloadImage(imagePath: imagePath)
        
        if product.subtype == "advert" {
            cartButton.isHidden = true
            dateLabel.isHidden = false
            
            if let time = (Double(product.timestamp)) {
                let date = Date(timeIntervalSince1970: time)
                let calendar = Calendar.current
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                dateLabel.text = dateFormatter.string(from: date)
                
                if calendar.isDateInYesterday(date) {
                    dateLabel.text = "Вчера, \(dateFormatter.string(from: date))"
                    
                } else if calendar.isDateInToday(date) {
                    dateLabel.text = "Сегодня, \(dateFormatter.string(from: date))"
                    
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd MMMM, HH:mm"
                    dateLabel.text = dateFormatter.string(from: date)
                }
            }
        }
    }
    
    
    func configure(ad: APIResponse.MyAdvertisements.Advertisements) {
        cartButton.isHidden = true
        trashIcon.isHidden = true
        statusLabel.isHidden = false
        statusLabel.text = ad.statusText
        nameLabel.text = ad.name
        
        let floatPrice = Float(ad.price)
        let floatOldPrice = Float(ad.oldPrice)
        setupLabels(floatOldPrice: floatOldPrice, floatPrice: floatPrice)
        
        if ad.discount != nil {
            badgeImageView.isHidden = false
            discountLabel.isHidden = false
            
            discountLabel.text = "\(ad.discount!) %"
        }
        
        guard let imagePath = ad.imagePath else { return }
        downloadImage(imagePath: imagePath)
    }
    
    
    @IBAction func deleteAction(_ sender: Any) {
        output?.delete(id: id)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        cartButton.isHidden = false
        dateLabel.isHidden = true
    }
    
    
    // MARK: - Helper
    
    private func setupLabels(floatOldPrice: Float, floatPrice: Float) {
        if floatOldPrice > Float(0) {
            let priceStr = String(format: "%.0f", floatPrice)
            let listPriceStr = String(format: "%.0f", floatOldPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr) ₽  \(listPriceStr) ₽", attributes: [
                .font: UIFont.systemFont(ofSize: 13.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08,
                ])
            attributedString.addAttributes([
                .font: UIFont.systemFont(ofSize: 15.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 250/255, green: 114/255, blue: 104/255, alpha: 1),
                ], range: NSRange(location: 0, length: listPriceStr.count + 2))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1,
                                          range: NSMakeRange(priceStr.count + 4, listPriceStr.count + 2))
            
            priceLabel.attributedText = attributedString
            
        } else {
            let priceStr = floatPrice == 0 ? "Бесплатно" : String(format: "%.0f ₽", floatPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr)", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08
                ])
            
            priceLabel.attributedText = attributedString
        }
    }
    
    private func downloadImage(imagePath: String) {
        productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        SDWebImageDownloader.shared.setValue("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                                             forHTTPHeaderField: "Accept")
        productImageView!.sd_setImage(with: URL(string: imagePath), placeholderImage: nil, options: [.continueInBackground, .highPriority], context: nil)
    }
}
