//
//  MPLMyMessageCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 17/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLMyMessageCell: MPLTableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var myNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var adNameLabel: UILabel!
    
    
    override class func cellHeight() -> CGFloat {
        return 80
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func configure(thread: APIResponse.ChatList.Thread) {
        myNameLabel.text = "\(thread.firstName) \(thread.lastName ?? "")"
        messageLabel.text = thread.lastMessage
        if let time = (Double(thread.lastUpdated)) {
            let date = Date(timeIntervalSince1970: time)
            let calendar = Calendar.current
            
            if calendar.isDateInYesterday(date) {
                dateLabel.text = "Вчера"
                
            } else if calendar.isDateInToday(date) {
                dateLabel.text = "Сегодня"
                
            } else {
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.short
                dateLabel.text = dateFormatter.string(from: date)
            }
        }
        
        guard let productName = thread.productName else { return }
        adNameLabel.text = productName
    }
}
