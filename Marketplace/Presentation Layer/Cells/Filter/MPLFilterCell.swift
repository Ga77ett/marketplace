//
//  MPLFilterCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 15/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLFilterCell: MPLTableViewCell {


    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var accessoryImageView: UIImageView!
    
    
    override class func cellHeight() -> CGFloat {
        return 60
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(filter: APIResponse.Filters.Filter) {
        nameLabel.text = filter.name
        placeholderLabel.text = filter.name
        
        if filter.displayType == MPLFilterDisplayType.switchType.rawValue {
            switchView.isHidden = false
            switchView.setOn(filter.variants?.first?.selected ?? false, animated: true)
            
        } else {
            accessoryImageView.isHidden = false
        }
        
        if filter.displayType == MPLFilterDisplayType.price.rawValue {
            if filter.selectedMin > 0 {
                nameLabel.isHidden = true
                placeholderLabel.isHidden = false
                descLabel.isHidden = false
                
                descLabel.text = "От \(filter.selectedMin) ₽"
            }
            
            if filter.selectedMax > 0 {
                nameLabel.isHidden = true
                placeholderLabel.isHidden = false
                descLabel.isHidden = false
                
                if descLabel.text?.count == 0 {
                    descLabel.text = "До \(filter.selectedMax) ₽"
                    
                } else {
                    descLabel.text! += ", до \(filter.selectedMax) ₽"
                }
            }
            
        } else if filter.displayType != MPLFilterDisplayType.switchType.rawValue {
            guard let variants = filter.variants else { return }
            for variant in variants {
                if variant.selected {
                    nameLabel.isHidden = true
                    placeholderLabel.isHidden = false
                    descLabel.isHidden = false
                    
                    if descLabel.text!.count == 0 {
                        descLabel.text = "\(variant.name)"
                        
                    } else {
                        descLabel.text = "\(descLabel.text!), \(variant.name)"
                    }
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        switchView.isHidden = true
        accessoryImageView.isHidden = true
        nameLabel.isHidden = false
        placeholderLabel.isHidden = true
        descLabel.isHidden = true
        nameLabel.text = ""
        descLabel.text = ""
        placeholderLabel.text = ""
    }
}
