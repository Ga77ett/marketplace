//
//  MPLCatalogCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 21/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SDWebImage

class MPLCatalogCell: MPLTableViewCell {
    
    @IBOutlet weak var catalogImageView: UIImageView!
    @IBOutlet weak var catalogName: UILabel!
    @IBOutlet weak var leadingCatalogImageViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    
    override class func cellHeight() -> CGFloat {
        return 60
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        //self.accessoryView = UIImageView.init(image: UIImage.init(named: "Accessory_icon"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        leadingCatalogImageViewConstraint.constant = 18
        catalogImageView.image = nil
        rightImageView.image = nil
    }
    
    
    // MARK: - Setup cell
    
    public func configure(catalog: APIResponse.Category) {
        catalogName.text = "\(catalog.name)"
        
        if catalog.hasChildren {
            rightImageView.image = UIImage.init(named: "Accessory_icon")

        } else {
            rightImageView.image = UIImage.init()
            countLabel.text = "\(catalog.productCount)"
        }
        
        if Int(catalog.parentID)! == 0 {
            guard let mainPair = catalog.mainPair else { return }
            catalogImageView.sd_setImage(with: URL.init(string: mainPair.detailed.imagePath), completed: nil)
            
        } else {
            leadingCatalogImageViewConstraint.constant = -catalogImageView.bounds.size.width + 4
        }
    }
}
