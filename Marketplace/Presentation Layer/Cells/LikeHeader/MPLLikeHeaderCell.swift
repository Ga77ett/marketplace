//
//  MPLLikeHeaderCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 19/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLLikeHeaderCell: MPLTableViewCell {

    @IBOutlet weak var label: UILabel!
    
    
    override class func cellHeight() -> CGFloat {
        return 44
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
