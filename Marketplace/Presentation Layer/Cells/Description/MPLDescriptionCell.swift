//
//  MPLDescriptionCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 26/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

protocol MPLDescriptionCellOutput {
    func needToReloadTable()
}

class MPLDescriptionCell: MPLTableViewCell {

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var lessMoreLabel: UILabel!
    public var output: MPLDescriptionCellOutput?
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(text: String, collapsed: Bool) {
        descLabel.text = text
        
        if collapsed {
            descLabel.numberOfLines = 5
            lessMoreLabel.text = "подробнее"
            
        } else {
            descLabel.numberOfLines = 0
            lessMoreLabel.text = "скрыть"
        }
    }
    
    @IBAction func collapseAction(_ sender: Any) {
        output?.needToReloadTable()
    }
}
