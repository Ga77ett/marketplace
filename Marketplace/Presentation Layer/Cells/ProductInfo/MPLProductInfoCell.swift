//
//  MPLProductInfoCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 26/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import Cosmos

class MPLProductInfoCell: MPLTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override class func cellHeight() -> CGFloat {
        return 120
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(product: APIResponse.Product) {
        nameLabel.text = product.name
        
        // TODO: Нужно сделать настройку цену один раз, а не копипаст
        let floatPrice = Float(product.price)
        let floatOldPrice = Float(product.oldPrice)
        
        if floatOldPrice > Float(0) {
            let priceStr = String(format: "%.0f", floatPrice)
            let listPriceStr = String(format: "%.0f", floatOldPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr) ₽  \(listPriceStr) ₽ (выгода \(Int(floatOldPrice - floatPrice)) ₽)", attributes: [
                .font: UIFont.systemFont(ofSize: 16.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08,
                ])
            attributedString.addAttributes([
                .font: UIFont.systemFont(ofSize: 20.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 250/255, green: 114/255, blue: 104/255, alpha: 1),
                ], range: NSRange(location: 0, length: listPriceStr.count + 2))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1,
                                          range: NSMakeRange(priceStr.count + 4, listPriceStr.count + 2))
            
            priceLabel.attributedText = attributedString
            
        } else {
            let priceStr = floatPrice == 0 ? "Бесплатно" : String(format: "%.0f ₽", floatPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr)", attributes: [
                .font: UIFont.systemFont(ofSize: 20.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08
                ])
            
            priceLabel.attributedText = attributedString
        }
        
        if product.subtype != "advert" {
            ratingView.rating = Double(product.averageRating ?? 0)
            ratingView.text = String(product.averageRating ?? 0)
            
        } else {
            ratingView.isHidden = true
            heightConstraint.constant = 0
            topConstraint.constant = 0
        }
    }
    
}
