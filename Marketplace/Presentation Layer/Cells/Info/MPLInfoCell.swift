//
//  MPLInfoCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 30/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

enum MPLInfoCellType: Int {
    case info           = 0
    case feedback       = 1
    case settings       = 2
    case about          = 3
    case specification  = 4
}

class MPLInfoCell: MPLTableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var accessoryImageView: UIImageView!
    
    
    override class func cellHeight() -> CGFloat {
        return 60
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    public func configureCell(type: MPLInfoCellType) {
        switch type {
        case .info:
            imageView?.image = UIImage.init(named: "Info_icon")
            headerLabel.text = "Информация"
            descLabel.text = "Покупателям, продавцам"
        case .feedback:
            imageView?.image = UIImage.init(named: "Feedback_icon")
            headerLabel.text = "Обратная связь"
            descLabel.text = "Телефон, e-mail, instagram и т.д."
        case .settings:
            imageView?.image = UIImage.init(named: "Settings_icon")
            headerLabel.text = "Настройки"
            descLabel.text = "Регион, уведомления"
        case .about:
            imageView?.image = UIImage.init(named: "About_icon")
            headerLabel.text = "О приложении"
            descLabel.text = "Доп. информация, номер версии"
        case .specification:
            imageView?.image = UIImage.init(named: "Info_icon")
            headerLabel.text = "Характеристики"
            descLabel.text = "Материл/Цвет/Габариты"
            accessoryImageView.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        accessoryImageView.isHidden = true
    }
}
