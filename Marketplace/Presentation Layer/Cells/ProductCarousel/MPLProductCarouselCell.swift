//
//  MPLProductCarouselCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 25/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProductCarouselCell: MPLTableViewCell {
    
    @IBOutlet weak var collectionView: MPLProductCarouselView!
    
    
    override class func cellHeight() -> CGFloat {
        return 230
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = collectionView
        collectionView.delegate = collectionView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(products: APIResponse.SpecialProducts, output: MPLListCellOutput) {
        collectionView.configure(products: products, output: output)
        
        collectionView.reloadData()
    }

}
