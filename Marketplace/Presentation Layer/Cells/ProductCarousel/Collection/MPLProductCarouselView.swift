//
//  MPLProductCarouselView.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 25/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProductCarouselView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private var data: APIResponse.SpecialProducts?
    private var output: MPLListCellOutput?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.register(UINib.init(nibName: MPLProductCollectionCell.cellNibName(), bundle: nil),
                      forCellWithReuseIdentifier: MPLProductCollectionCell.cellIdentifier())
    }

    public func configure(products: APIResponse.SpecialProducts, output: MPLListCellOutput) {
        data = products
        self.output = output
    }

    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.products.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MPLProductCollectionCell.cellIdentifier(),
                                                      for: indexPath) as! MPLProductCollectionCell
        let product = data!.products[indexPath.row]
        cell.configure(product: product)
        cell.output = output
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = data!.products[indexPath.row]
        output?.productTapped(id: product.id, name: product.name)
    }
}
