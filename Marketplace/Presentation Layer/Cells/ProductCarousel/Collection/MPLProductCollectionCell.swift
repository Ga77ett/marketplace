//
//  MPLProductCollectionCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 25/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLProductCollectionCell: UICollectionViewCell {
    
    // MARK: - Public vars & lets
    
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var favouritesButton: UIButton!
    public var output: MPLListCellOutput?
    
    
    // MARK: - Private vars & lets
    
    private var id: String = ""
    private var inWishlist: Bool = false
    
    
    class func cellHeight() -> CGFloat {
        return 215
    }
    
    class func cellNibName() -> String {
        return cellIdentifier()
    }
    
    class func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.cornerRadius = 10
        frameView.clipsToBounds = true
        MPLAppearance.addShadow(view: frameView)
    }
    
    func configure(product: APIResponse.Product) {
        self.id = product.id
        self.inWishlist = product.inWishlist!
        
        nameLabel.text = product.name

        if product.oldPrice > 0 {
            let priceStr = String(format: "%.0f", product.price)
            let listPriceStr = String(format: "%.0f", product.oldPrice)
            let attributedString = NSMutableAttributedString(string: "\(priceStr) ₽  \(listPriceStr) ₽", attributes: [
                .font: UIFont.systemFont(ofSize: 13.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08,
                ])
            attributedString.addAttributes([
                .font: UIFont.systemFont(ofSize: 15.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 250/255, green: 114/255, blue: 104/255, alpha: 1),
                ], range: NSRange(location: 0, length: listPriceStr.count + 2))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1,
                                          range: NSMakeRange(priceStr.count + 4, listPriceStr.count + 2))

            priceLabel.attributedText = attributedString

        } else {
            let priceStr = String(format: "%.0f", product.price)
            let attributedString = NSMutableAttributedString(string: "\(priceStr) ₽", attributes: [
                .font: UIFont.systemFont(ofSize: 15.0, weight: .medium),
                .foregroundColor: UIColor.init(red: 127/255, green: 133/255, blue: 146/255, alpha: 1),
                .kern: -0.08
                ])

            priceLabel.attributedText = attributedString
        }
        
        if product.discount != nil {
            badgeImageView.isHidden = false
            discountLabel.isHidden = false
            
            discountLabel.text = "\(product.discount!) %"
        }
        
        if product.inWishlist! {
            favouritesButton.setImage(UIImage.init(named: "Favourites_active"), for: .normal)
            
        } else {
            favouritesButton.setImage(UIImage.init(named: "Favourites_inactive"), for: .normal)
        }
        
        guard let imagePath = product.imagePath else { return }
        productImageView.sd_setImage(with: URL.init(string: imagePath), completed: nil)
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        badgeImageView.isHidden = true
        discountLabel.isHidden = true
    }
    
    @IBAction func favouritesAction(_ sender: Any) {
        inWishlist = !inWishlist
        
        output?.favourites(id: id)
        
        if inWishlist {
            favouritesButton.setImage(UIImage.init(named: "Favourites_active"), for: .normal)
            
        } else {
            favouritesButton.setImage(UIImage.init(named: "Favourites_inactive"), for: .normal)
        }
    }
}
