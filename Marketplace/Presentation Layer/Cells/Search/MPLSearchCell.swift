//
//  MPLSearchCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 22/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSearchCell: MPLTableViewCell {
    
    @IBOutlet weak var checkboxImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectionImageView: UIImageView!
    
    override class func cellHeight() -> CGFloat {
        return 60
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(variant: APIResponse.Filters.Filter.Variant, single: Bool) {
        leftConstraint.constant = single ? -22 : 20
        
        if single {
            selectionImageView.isHidden = false
            
        } else {
            checkboxImageView.isHidden = false
        }
        
        if variant.selected {
            checkboxImageView.image = UIImage.init(named: "Checkbox_selected")
            selectionImageView.image = UIImage.init(named: "Selection_icon")
            
        } else {
            checkboxImageView.image = UIImage.init(named: "Checkbox_unselected")
            selectionImageView.image = UIImage.init(named: "")
        }
        
        label.text = variant.name
    }
    
    func configure(city: APIResponse.Cities.City) {
        leftConstraint.constant = -22
        selectionImageView.isHidden = false
        label.text = city.name
        
        if city.selected {
            selectionImageView.image = UIImage.init(named: "Selection_icon")
            
        } else {
            selectionImageView.image = UIImage.init(named: "")
        }
    }
}
