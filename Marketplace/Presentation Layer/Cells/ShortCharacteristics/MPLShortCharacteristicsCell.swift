//
//  MPLShortCharacteristicsCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 27/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

enum MPLShortCharacteristicsCellType: Int {
    case product = 0
    case advert  = 1
}

class MPLShortCharacteristicsCell: MPLTableViewCell {
    
    @IBOutlet weak var topLeftLabel: UILabel!
    @IBOutlet weak var centerLeftLabel: UILabel!
    @IBOutlet weak var bottomLeftLabel: UILabel!
    @IBOutlet weak var topRightLabel: UILabel!
    @IBOutlet weak var centerRightLabel: UILabel!
    @IBOutlet weak var bottomRightLabel: UILabel!
    
    
    override class func cellHeight() -> CGFloat {
        return 126
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(topText: String, centerText: String, bottomText: String, type: MPLShortCharacteristicsCellType) {
        if type == .product {
            centerLeftLabel.text  = "Продавец"
            bottomLeftLabel.text  = "Артикул"
            bottomRightLabel.text = bottomText
            
        } else {
            centerLeftLabel.text  = "Продавец"
            bottomLeftLabel.text  = "Размещено"
                
            if let time = (Double(bottomText)) {
                let date = Date(timeIntervalSince1970: time)
                let calendar = Calendar.current
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                bottomRightLabel.text = dateFormatter.string(from: date)
                
                if calendar.isDateInYesterday(date) {
                    bottomRightLabel.text = "Вчера, \(dateFormatter.string(from: date))"
                    
                } else if calendar.isDateInToday(date) {
                    bottomRightLabel.text = "Сегодня, \(dateFormatter.string(from: date))"
                    
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd MMMM, HH:mm"
                    bottomRightLabel.text = dateFormatter.string(from: date)
                }
            }
        }
        
        topRightLabel.text      = topText
        centerRightLabel.text   = centerText
    }
}
