//
//  MPLBannersCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 01/07/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import ImageSlideshow
import AlamofireImage

class MPLBannersCell: MPLTableViewCell {
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    
    
    override class func cellHeight() -> CGFloat {
        return 175
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        slideShow.layer.cornerRadius = 5
        slideShow.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func configure(banners: APIResponse.BannerList) {
        slideShow.contentScaleMode = .scaleAspectFill
        slideShow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .customBottom(padding: 35))
        slideShow.slideshowInterval = 5
        
        var array: [InputSource] = []
        
        for banner in banners.banners {
            array.append(AlamofireSource(urlString: banner.mainPair.icon.imagePath)!)
        }
        
        slideShow.setImageInputs(array)
    }
    
}
