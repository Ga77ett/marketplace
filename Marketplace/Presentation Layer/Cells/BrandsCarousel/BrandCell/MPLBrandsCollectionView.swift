//
//  MPLBrandsCollectionView.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 27/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLBrandsCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource {

    private var data: APIResponse.BrandList?
    private var output: MPLBrandsCarouselCellOutput?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.register(UINib.init(nibName: MPLBrandCell.cellNibName(), bundle: nil),
                      forCellWithReuseIdentifier: MPLBrandCell.cellIdentifier())
    }
    
    public func configure(brands: APIResponse.BrandList, output: MPLBrandsCarouselCellOutput) {
        data = brands
        self.output = output
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.brands.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MPLBrandCell.cellIdentifier(),
                                                      for: indexPath) as! MPLBrandCell
        let brand = data!.brands[indexPath.row]
        cell.configure(brand: brand)
        return cell
    }

    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let brand = data!.brands[indexPath.row]
        output?.brandsTapped(variantID: brand.variantID, featureID: brand.featureID, name: brand.name)
    }
}
