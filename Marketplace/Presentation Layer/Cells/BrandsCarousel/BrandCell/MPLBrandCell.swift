//
//  MPLBrandCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 27/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLBrandCell: UICollectionViewCell {
    
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var brandImageView: UIImageView!
    
    
    class func cellHeight() -> CGFloat {
        return 110
    }
    
    class func cellNibName() -> String {
        return cellIdentifier()
    }
    
    class func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.cornerRadius = 10
        frameView.clipsToBounds = true
    }
    
    func configure(brand: APIResponse.BrandList.Brand) {
        brandImageView.sd_setImage(with: URL.init(string: brand.imagePair.icon.imagePath), completed: nil)
        
        MPLAppearance.addShadow(view: frameView)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
