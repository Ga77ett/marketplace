//
//  MPLBrandsCarouselCell.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 27/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

protocol MPLBrandsCarouselCellOutput {
    func brandsTapped(variantID: String, featureID: String, name: String)
}

class MPLBrandsCarouselCell: MPLTableViewCell {

    @IBOutlet weak var brandsCollectionView: MPLBrandsCollectionView!
    var output: MPLBrandsCarouselCellOutput?
    
    
    override class func cellHeight() -> CGFloat {
        return 140
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        brandsCollectionView.dataSource = brandsCollectionView
        brandsCollectionView.delegate = brandsCollectionView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(brands: APIResponse.BrandList, output: MPLBrandsCarouselCellOutput) {
        brandsCollectionView.configure(brands: brands, output: output)
        brandsCollectionView.reloadData()
    }
}
