//
//  MPLRapalaxTableView.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 18/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import ImageSlideshow
import AlamofireImage

class MPLParalaxTableView: UITableView {

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var searchButton: UIButton!
    
    
    override func draw(_ rect: CGRect) {
        searchView.layer.cornerRadius = 10
        searchView.clipsToBounds = true
        MPLAppearance.addShadow(view: searchView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let header = tableHeaderView else { return }
        
        let offsetY = -contentOffset.y
        heightConstraint.constant = max(header.bounds.height, header.bounds.height + offsetY)
        
        /*
         guard let header = tableHeaderView else { return }
         
         if let imageView = header.subviews.first as? UIImageView {
         heightConstraint = header.constraints.filter{ $0.identifier == "heightConstraint" }.first
         bottomConstraint = header.constraints.filter{ $0.identifier == "bottomConstraint" }.first
         }
         
         let offsetY = -contentOffset.y
         heightConstraint?.constant = max(header.bounds.height, header.bounds.height + offsetY)
         bottomConstraint?.constant = offsetY >= 0 ? 0: offsetY / 2
         
         header.clipsToBounds = offsetY <= 0
         */
    }
    
    public func configureSlider(banners: APIResponse.BannerList) {
        slideShow.contentScaleMode = .scaleAspectFill
        slideShow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .customBottom(padding: 35))
        slideShow.slideshowInterval = 5

        var array: [InputSource] = []

        for banner in banners.banners {
            array.append(AlamofireSource(urlString: banner.mainPair.icon.imagePath)!)
        }

        slideShow.setImageInputs(array)
    }
}
