//
//  MPLButton.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 25
        clipsToBounds = true
        MPLAppearance.addShadow(view: self)
    }

}
