//
//  MPLErrorMessage.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 05/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLErrorMessage: UIView {
    
    @IBOutlet weak var errorLabel: UILabel!
    
    
    class func instanceFromNib() -> MPLErrorMessage {
        return UINib(nibName: "MPLErrorMessage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MPLErrorMessage
    }
    
    public func show(message: String) {
        let window = UIApplication.shared.keyWindow!
        
        errorLabel.numberOfLines = 0
        errorLabel.text = message
        errorLabel.textColor = UIColor.white
        errorLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        
        window.addSubview(self)
        self.frame = CGRect.init(x: 0, y: -self.bounds.size.height, width: window.bounds.size.width, height: self.bounds.size.height)
        self.layoutIfNeeded()
        showAnimation()
    }
    
    
    private func showAnimation() {
        UIView.animate(withDuration: 0.5, animations: {
            self.frame = CGRect.init(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        }) { (_) in
            self.remove()
        }
    }
    
    private func remove() {
        UIView.animate(withDuration: 0.3, delay: 4.0, options: [], animations: {
            self.frame = CGRect.init(x: 0, y: -self.bounds.size.height, width: self.bounds.size.width, height: self.bounds.size.height)
            
        }) { (finished) in
            self.removeFromSuperview()
        }
    }

}
