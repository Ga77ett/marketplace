//
//  MPLActivityView.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 02/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class MPLActivityView: UIView {
    
    @IBOutlet weak var activity: NVActivityIndicatorView!
    
    
    class func instanceFromNib() -> MPLActivityView {
        return UINib(nibName: "MPLActivityView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MPLActivityView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func show() {
        if let topVC = UIApplication.getTopViewController() {
            self.center = topVC.view.center
            //print(self.center)
            topVC.view.addSubview(self)
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        activity.startAnimating()
    }
    
    public func hide() {
        activity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        self.removeFromSuperview()
    }
}
