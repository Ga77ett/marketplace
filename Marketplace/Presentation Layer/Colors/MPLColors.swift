//
//  MPLColors.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 11/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

extension UIColor {
    @nonobjc class var white: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
    @nonobjc class var silver: UIColor {
        return UIColor(red: 202.0 / 255.0, green: 207.0 / 255.0, blue: 210.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteTwo: UIColor {
        return UIColor(white: 238.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pumpkinOrange: UIColor {
        return UIColor(red: 251.0 / 255.0, green: 124.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var blackTwo: UIColor {
        return UIColor(white: 34.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var brightBlue: UIColor {
        return UIColor(red: 0.0, green: 122.0 / 255.0, blue: 1.0, alpha: 1.0)
    }
    @nonobjc class var slateGrey: UIColor {
        return UIColor(red: 110.0 / 255.0, green: 110.0 / 255.0, blue: 112.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyTwo: UIColor {
        return UIColor(white: 153.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyThree: UIColor {
        return UIColor(white: 151.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var silver40: UIColor {
        return UIColor(red: 202.0 / 255.0, green: 207.0 / 255.0, blue: 210.0 / 255.0, alpha: 0.4)
    }
    @nonobjc class var pumpkinOrangeTwo: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 120.0 / 255.0, blue: 3.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteThree: UIColor {
        return UIColor(white: 216.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var coolGrey80: UIColor {
        return UIColor(red: 165.0 / 255.0, green: 171.0 / 255.0, blue: 181.0 / 255.0, alpha: 0.8)
    }
    @nonobjc class var white95: UIColor {
        return UIColor(white: 1.0, alpha: 0.95)
    }
    @nonobjc class var perrywinkle: UIColor {
        return UIColor(red: 126.0 / 255.0, green: 187.0 / 255.0, blue: 231.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var lightBlueGrey25: UIColor {
        return UIColor(red: 163.0 / 255.0, green: 200.0 / 255.0, blue: 229.0 / 255.0, alpha: 0.25)
    }
    @nonobjc class var vermillion: UIColor {
        return UIColor(red: 239.0 / 255.0, green: 29.0 / 255.0, blue: 29.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var silverTwo: UIColor {
        return UIColor(red: 209.0 / 255.0, green: 213.0 / 255.0, blue: 219.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var slateGreyTwo: UIColor {
        return UIColor(red: 104.0 / 255.0, green: 104.0 / 255.0, blue: 113.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var brownishGrey: UIColor {
        return UIColor(white: 97.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var orangeRed: UIColor {
        return UIColor(red: 1.0, green: 59.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var golden: UIColor {
        return UIColor(red: 241.0 / 255.0, green: 190.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyFour: UIColor {
        return UIColor(white: 140.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var red: UIColor {
        return UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var silver85: UIColor {
        return UIColor(red: 201.0 / 255.0, green: 206.0 / 255.0, blue: 214.0 / 255.0, alpha: 0.85)
    }
    @nonobjc class var leafyGreen: UIColor {
        return UIColor(red: 71.0 / 255.0, green: 192.0 / 255.0, blue: 76.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var blackThree: UIColor {
        return UIColor(white: 42.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pumpkin: UIColor {
        return UIColor(red: 240.0 / 255.0, green: 130.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var silver90: UIColor {
        return UIColor(red: 220.0 / 255.0, green: 220.0 / 255.0, blue: 222.0 / 255.0, alpha: 0.9)
    }
    @nonobjc class var pinkishGreyTwo: UIColor {
        return UIColor(white: 206.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteFour: UIColor {
        return UIColor(white: 224.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var blackFour: UIColor {
        return UIColor(white: 51.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var milkChocolate: UIColor {
        return UIColor(red: 113.0 / 255.0, green: 72.0 / 255.0, blue: 32.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteFive: UIColor {
        return UIColor(white: 229.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyFive: UIColor {
        return UIColor(white: 130.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var turtleGreen: UIColor {
        return UIColor(red: 119.0 / 255.0, green: 189.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGreyThree: UIColor {
        return UIColor(red: 202.0 / 255.0, green: 197.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var denimBlue: UIColor {
        return UIColor(red: 59.0 / 255.0, green: 89.0 / 255.0, blue: 152.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var white50: UIColor {
        return UIColor(white: 1.0, alpha: 0.5)
    }
    @nonobjc class var black70: UIColor {
        return UIColor(white: 0.0, alpha: 0.7)
    }
    @nonobjc class var lightblue: UIColor {
        return UIColor(red: 125.0 / 255.0, green: 185.0 / 255.0, blue: 233.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var dullBlue: UIColor {
        return UIColor(red: 77.0 / 255.0, green: 118.0 / 255.0, blue: 161.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var blackFive: UIColor {
        return UIColor(white: 3.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteSix: UIColor {
        return UIColor(white: 235.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteSeven: UIColor {
        return UIColor(white: 253.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteEight: UIColor {
        return UIColor(white: 233.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var dullYellow: UIColor {
        return UIColor(red: 228.0 / 255.0, green: 213.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreySix: UIColor {
        return UIColor(white: 116.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var lightGrey: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 243.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreySeven: UIColor {
        return UIColor(red: 155.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteNine: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 237.0 / 255.0, blue: 237.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var mediumPink: UIColor {
        return UIColor(red: 239.0 / 255.0, green: 59.0 / 255.0, blue: 162.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyEight: UIColor {
        return UIColor(white: 119.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var white0: UIColor {
        return UIColor(white: 1.0, alpha: 0.0)
    }
    @nonobjc class var mudBrown: UIColor {
        return UIColor(red: 77.0 / 255.0, green: 21.0 / 255.0, blue: 21.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyNine: UIColor {
        return UIColor(white: 155.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGreyFour: UIColor {
        return UIColor(red: 226.0 / 255.0, green: 205.0 / 255.0, blue: 205.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var blackSix: UIColor {
        return UIColor(white: 45.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var steelGrey: UIColor {
        return UIColor(red: 128.0 / 255.0, green: 130.0 / 255.0, blue: 133.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var bluish95: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 127.0 / 255.0, blue: 193.0 / 255.0, alpha: 0.95)
    }
    @nonobjc class var windowsBlue: UIColor {
        return UIColor(red: 56.0 / 255.0, green: 137.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var dusk: UIColor {
        return UIColor(red: 59.0 / 255.0, green: 67.0 / 255.0, blue: 113.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkSkyBlue: UIColor {
        return UIColor(red: 58.0 / 255.0, green: 162.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGreyFive: UIColor {
        return UIColor(red: 192.0 / 255.0, green: 185.0 / 255.0, blue: 185.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGreySix: UIColor {
        return UIColor(white: 196.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGreySeven: UIColor {
        return UIColor(red: 192.0 / 255.0, green: 186.0 / 255.0, blue: 186.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGreyTen: UIColor {
        return UIColor(white: 115.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var rustyRed: UIColor {
        return UIColor(red: 172.0 / 255.0, green: 17.0 / 255.0, blue: 17.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var brownishGreyTwo: UIColor {
        return UIColor(white: 104.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteTen: UIColor {
        return UIColor(white: 221.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var milkChocolateTwo: UIColor {
        return UIColor(red: 83.0 / 255.0, green: 32.0 / 255.0, blue: 32.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var azure: UIColor {
        return UIColor(red: 29.0 / 255.0, green: 161.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkCoral: UIColor {
        return UIColor(red: 207.0 / 255.0, green: 77.0 / 255.0, blue: 77.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var black75: UIColor {
        return UIColor(white: 0.0, alpha: 0.75)
    }
    @nonobjc class var coolGrey: UIColor {
        return UIColor(red: 173.0 / 255.0, green: 176.0 / 255.0, blue: 181.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var deepSkyBlue: UIColor {
        return UIColor(red: 21.0 / 255.0, green: 126.0 / 255.0, blue: 251.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var heliotrope: UIColor {
        return UIColor(red: 216.0 / 255.0, green: 83.0 / 255.0, blue: 234.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteEleven: UIColor {
        return UIColor(white: 217.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var black: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }
    @nonobjc class var warmGrey: UIColor {
        return UIColor(white: 136.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var bluish: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 127.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var lightKhaki: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 232.0 / 255.0, blue: 160.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var dullYellowTwo: UIColor {
        return UIColor(red: 228.0 / 255.0, green: 213.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGrey: UIColor {
        return UIColor(white: 204.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var grassGreen: UIColor {
        return UIColor(red: 67.0 / 255.0, green: 171.0 / 255.0, blue: 20.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var bluish90: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 127.0 / 255.0, blue: 193.0 / 255.0, alpha: 0.9)
    }
    @nonobjc class var greyish: UIColor {
        return UIColor(white: 173.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var pumpkinOrangeThree: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 115.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var steel: UIColor {
        return UIColor(red: 127.0 / 255.0, green: 133.0 / 255.0, blue: 146.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var salmon: UIColor {
        return UIColor(red: 250.0 / 255.0, green: 114.0 / 255.0, blue: 104.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkBlueGrey: UIColor {
        return UIColor(red: 27.0 / 255.0, green: 39.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmBlue: UIColor {
        return UIColor(red: 53.0 / 255.0, green: 98.0 / 255.0, blue: 211.0 / 255.0, alpha: 1.0)
    }
}
