//
//  MPLChatViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 18/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import Alamofire
import InputBarAccessoryView

class MPLChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Private vars & lets
    
    private var data: APIResponse.Chat?
    private let inputBar: InputBarAccessoryView = iMessageInputBar.init()
    private var keyboardManager = KeyboardManager()
    private let barHeight: CGFloat = 54
    
    
    // MARK: - Public vars & lets
    
    @IBOutlet weak var tableView: UITableView!
    public var thread: APIResponse.ChatList.Thread?
    

    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupInitialState()
        downloadMessages()
    }
    
    private func setupInitialState() {
        title = "\(thread?.firstName ?? "") \(thread?.lastName ?? "")"
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        setupTableView()
        setupInputBar()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: MPLIncomingCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLIncomingCell.cellIdentifier())
        tableView.register(UINib(nibName: MPLOutgoingCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLOutgoingCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.keyboardDismissMode = .interactive
        tableView.contentInset.bottom = barHeight
    }
    
    private func setupInputBar() {
        inputBar.delegate = self
        view.addSubview(inputBar)
        
        // Binding the inputBar will set the needed callback actions to position the inputBar on top of the keyboard
        keyboardManager.bind(inputAccessoryView: inputBar)
        
        // Binding to the tableView will enabled interactive dismissal
        keyboardManager.bind(to: tableView)
        
        // Add some extra handling to manage content inset
        keyboardManager.on(event: .didChangeFrame) { [weak self] (notification) in
            self?.tableView.contentInset.bottom = notification.endFrame.height - 36
            self?.tableView.scrollIndicatorInsets.bottom = notification.endFrame.height - 36
            self?.tableView.scrollToBottomRow(animated: true)
            
            }.on(event: .didHide) { [weak self] _ in
                self?.tableView.contentInset.bottom = self!.barHeight
                self?.tableView.scrollIndicatorInsets.bottom = self!.barHeight
                self?.tableView.scrollToBottomRow(animated: true)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.tableView.contentInset.bottom = self.barHeight
            self.tableView.scrollToBottomRow(animated: true)
        }
    }
    

    // MARK: - UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.messages.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let thread = data!.messages[indexPath.row]
        if thread.isMyMessage {
            let cell = tableView.dequeueReusableCell(withIdentifier: MPLOutgoingCell.cellIdentifier(), for: indexPath) as! MPLOutgoingCell
            cell.configure(thread: thread)
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MPLIncomingCell.cellIdentifier(), for: indexPath) as! MPLIncomingCell
            cell.configure(thread: thread)
            
            return cell
        }
    }
    
    
    // MARK: - Network
    
    private func downloadMessages() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.getChat,
                        params: ["client_token" : token, "thread_id" : thread?.id ?? ""]) {
                            (res: Swift.Result<APIResponse.Chat, AlertMessage>) in
                            switch res {
                            case .success(let response):
                                self.data = nil
                                self.data = response
                                self.tableView.reloadData()
                                self.tableView.scrollToBottomRow(animated: true)
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
    
    private func sendMessage(message: String) {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.sendMessage,
                        params: ["client_token" : token, "object_type" : thread?.objectType ?? "",
                                 "object_id" : thread?.objectID ?? "", "message" : message]) {
                            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                            switch res {
                            case .success(let response):
                                DispatchQueue.main.async { [weak self] in
                                    self?.inputBar.sendButton.stopAnimating()
                                }

                                if response.errorCode == 0 {
                                    DispatchQueue.main.async { [weak self] in
                                        self?.inputBar.inputTextView.text = ""
                                        let message = APIResponse.Chat.Message.init(isMyMessage: true, message: message,
                                                                                    timestamp: "\(Date().timeIntervalSince1970)")
                                        self?.data?.messages.append(message)
                                        let indexPath = IndexPath(row: (self?.data?.messages.count ?? 1) - 1, section: 0)
                                        self?.tableView.insertRows(at: [indexPath], with: .automatic)
                                        self?.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                                    }
                                    
                                } else {
                                    let errorView = MPLErrorMessage.instanceFromNib()
                                    errorView.show(message: response.errorMessage!)
                                }
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
}

extension MPLChatViewController: InputBarAccessoryViewDelegate {
    
    // MARK: - InputBarAccessoryViewDelegate
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        inputBar.sendButton.startAnimating()
        self.sendMessage(message: text)
    }
    
    func inputBar(_ inputBar: InputBarAccessoryView, didChangeIntrinsicContentTo size: CGSize) {
        // Adjust content insets
        print(size)
        tableView.contentInset.bottom = size.height + 300 // keyboard size estimate
    }
    
}
