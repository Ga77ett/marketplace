//
//  MPLRegistrationViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class MPLRegistrationViewController: UIViewController {

    // MARK: - Public vars & lets
    
    @IBOutlet weak var nameField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passField: SkyFloatingLabelTextField!
    @IBOutlet weak var passAgainField: SkyFloatingLabelTextField!
    @IBOutlet weak var errorPassAgainLabel: UILabel!
    @IBOutlet weak var errorEmailLabel: UILabel!
    @IBOutlet weak var errorPassLabel: UILabel!
    @IBOutlet weak var errorNameLabel: UILabel!
    @IBOutlet weak var eulaLabel: UILabel!
    
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        setupInitialState()
    }
    
    private func setupInitialState() {
        setupEULA()
        setupToolbar()
    }
    
    private func setupEULA() {
        let attributedString = NSMutableAttributedString(string: "Нажимая “Зарегестрироваться”, я соглашаюсь с пользовательским соглашением и с обработкой моей персональной информации на условиях политики конфиденциальности", attributes: [
            .font: UIFont.systemFont(ofSize: 11.0, weight: .regular),
            .foregroundColor: UIColor.steel,
            .kern: -0.08
            ])
        attributedString.addAttributes([
            .font: UIFont.systemFont(ofSize: 12),
            .foregroundColor: UIColor.darkBlueGrey,
            .kern: 0.4
            ], range: NSRange(location: 45, length: 28))
        attributedString.addAttributes([
            .font: UIFont.systemFont(ofSize: 12),
            .foregroundColor: UIColor.darkBlueGrey,
            .kern: 0.4
            ], range: NSRange(location: 130, length: 27))
        eulaLabel.attributedText = attributedString
        eulaLabel.sizeToFit()
    }
    
    private func setupToolbar() {
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
                         UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDidTapped))]
        toolbar.sizeToFit()
        
        nameField.inputAccessoryView = toolbar
        emailField.inputAccessoryView = toolbar
        passField.inputAccessoryView = toolbar
        passAgainField.inputAccessoryView = toolbar
    }
    
    @objc func doneDidTapped() {
        nameField.resignFirstResponder()
        emailField.resignFirstResponder()
        passField.resignFirstResponder()
        passAgainField.resignFirstResponder()
    }
    
    
    // MARK: - Actions

    @IBAction func doneAction(_ sender: Any) {
        // Email
        guard let email = emailField.text else { return }
        var emailIsValid = false
        if email.isEmpty || !MPLValidation.validate(email: email) {
            errorEmailLabel.isHidden = false
            emailField.toogleErrorState(error: true)
            
        } else {
            errorEmailLabel.isHidden = true
            emailField.toogleErrorState(error: false)
            emailIsValid = true
        }
        
        // Password
        guard let pass = passField.text else { return }
        var passIsValid = false
        if pass.isEmpty {
            errorPassLabel.isHidden = false
            passField.toogleErrorState(error: true)
            
        } else {
            errorPassLabel.isHidden = true
            passField.toogleErrorState(error: false)
            passIsValid = true
        }
        
        // Password again
        guard let passAgain = passAgainField.text else { return }
        var passAgainIsValid = false
        if passAgain.isEmpty || passAgain != pass {
            errorPassAgainLabel.isHidden = false
            passAgainField.toogleErrorState(error: true)
            
        } else {
            errorPassAgainLabel.isHidden = true
            passAgainField.toogleErrorState(error: false)
            passAgainIsValid = true
        }
        
        // Name
        guard let name = nameField.text else { return }
        var nameIsValid = false
        if name.isEmpty {
            errorNameLabel.isHidden = false
            nameField.toogleErrorState(error: true)
            
        } else {
            errorNameLabel.isHidden = true
            nameField.toogleErrorState(error: false)
            nameIsValid = true
        }
        
        if emailIsValid && passIsValid && passAgainIsValid && nameIsValid {
            createProfile()
        }
    }
    
    @IBAction func secureAction(_ sender: Any) {
        passField.isSecureTextEntry.toggle()
        passAgainField.isSecureTextEntry.toggle()
    }
    
    
    // MARK: - Network
    
    private func createProfile() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        apiManager.call(type: RequestItemsType.createProfile,
                        params: ["user_data[firstname]" : nameField.text!,
                                 "user_data[email]" : emailField.text!,
                                 "user_data[password]" : passField.text!]) {
            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
            switch res {
            case .success(let response):
                if response.errorCode == 0 {
                    self.showAlert()
                } else {
                    let errorView = MPLErrorMessage.instanceFromNib()
                    errorView.show(message: response.errorMessage!)
                }

            case .failure(let message):
                print(message.body)
                break
            }
        }
    }
    
    
    // MARK: - Helper
    
    private func showAlert() {
        let alert = UIAlertController(title: "Проверьте почту",
                                      message: "Мы отправили ссылку для подтверждения учетной записи на \(emailField.text!)",
            preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Продолжить",
                                      style: UIAlertAction.Style.default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
