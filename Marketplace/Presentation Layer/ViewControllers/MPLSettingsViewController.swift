//
//  MPLSettingsViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 28/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLSettingsViewController: UITableViewController, MPLCitiesModuleOutput {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
    }
    
    private func setupInitialState() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLSettingsCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLSettingsCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLSettingsCell.cellIdentifier(), for: indexPath) as! MPLSettingsCell
        cell.configure(type: .city)
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLSettingsCell.cellHeight()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showCities()
    }
    
    
    // MARK: - Routes
    
    private func showCities() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLCitiesViewController") as! MPLCitiesViewController
            let presenter = controller.output as! MPLCitiesPresenter
            let cityID = UserDefaults.standard.string(forKey: kMPLDefaultCityID) ?? nil
            presenter.configure(cityID: cityID, output: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    // MARK: - MPLCitiesModuleOutput
    
    func citySelected(cityID: String, name: String) {
        UserDefaults.standard.set(cityID, forKey: kMPLDefaultCityID)
        UserDefaults.standard.set(name, forKey: kMPLDefaultCityName)
        UserDefaults.standard.synchronize()
        tableView.reloadData()
    }
}
