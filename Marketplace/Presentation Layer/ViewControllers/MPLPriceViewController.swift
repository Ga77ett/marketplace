//
//  MPLPriceViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 26/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol MPLPriceViewControllerOutput {
    func priceWasSelected(filter: APIResponse.Filters.Filter)
}

class MPLPriceViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Public vars & lets
    
    @IBOutlet weak var fromTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var toTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var doneButtonBottomConstraint: NSLayoutConstraint!
    public var output: MPLPriceViewControllerOutput?
    
    
    // MARK: - Private vars & lets
    
    private var filter: APIResponse.Filters.Filter?
    
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    func setupInitialState() {
        setupFields()
        setupButton()
    }
    
    func setupFields() {
        fromTextField.placeholderFont = UIFont.systemFont(ofSize: 15, weight: .regular)
        fromTextField.titleFont = UIFont.systemFont(ofSize: 13, weight: .medium)
        fromTextField.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        fromTextField.tintColor = UIColor.salmon
        fromTextField.delegate = self
        toTextField.placeholderFont = UIFont.systemFont(ofSize: 15, weight: .regular)
        toTextField.titleFont = UIFont.systemFont(ofSize: 13, weight: .medium)
        toTextField.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        toTextField.tintColor = UIColor.salmon
        toTextField.delegate = self
        
        guard let filter = self.filter else { return }
        fromTextField.placeholder = "От \(String(describing: filter.min!)) ₽"
        toTextField.placeholder = "До \(String(describing: filter.max!)) ₽"
        
        if filter.selectedMin > 0 {
            fromTextField.text = "\(String(describing: filter.selectedMin))"
        }
        
        if filter.selectedMax > 0 {
            toTextField.text = "\(String(describing: filter.selectedMax))"
        }
    }
    
    private func setupButton() {
        doneButton.layer.cornerRadius = 25
        doneButton.clipsToBounds = true
        MPLAppearance.addShadow(view: doneButton)
        
        doneButton.addTarget(self, action: #selector(doneDidTapped), for: .touchDown)
    }
    
    public func configure (filter: APIResponse.Filters.Filter, output: MPLPriceViewControllerOutput) {
        self.filter = filter
        self.output = output
    }
    
    @objc func doneDidTapped() {
        filter?.selectedMin = Int(fromTextField.text ?? "0") ?? 0
        filter?.selectedMax = Int(toTextField.text ?? "0") ?? 0
        output?.priceWasSelected(filter: filter!)
        navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Keyboard
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            
            let isKeyboardShowing = notification.name.rawValue == "UIKeyboardWillShowNotification"
            
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.doneButtonBottomConstraint.constant = isKeyboardShowing ? keyboardFrame!.height + 20 : 30
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true

        } else {
            return string.rangeOfCharacter(from: CharacterSet(charactersIn: "1234567890")) == nil ? false : true
        }
    }
}
