//
//  MPLResetPassViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class MPLResetPassViewController: UIViewController {
    
    // MARK: - Public vars & lets

    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    // MARK: - Private vars & lets
        
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    
    // MARK: - Actions

    @IBAction func doneAction(_ sender: Any) {
        guard let email = emailField.text else { return }
        if email.isEmpty || !MPLValidation.validate(email: email) {
            errorLabel.isHidden = false
            emailField.toogleErrorState(error: true)
            
        } else {
            errorLabel.isHidden = true
            emailField.toogleErrorState(error: false)
            sendPassword()
        }
    }
    
    
    // MARK: - Network
    
    private func sendPassword() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        apiManager.call(type: RequestItemsType.resetPass, params: ["user_email" : emailField.text!]) {
            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                switch res {
                case .success(let response):
                    if response.errorCode == 0 {
                        self.errorLabel.isHidden = true
                        self.emailField.toogleErrorState(error: false)
                        self.showAlert()
                        
                    } else {
                        self.emailField.toogleErrorState(error: true)
                        let errorView = MPLErrorMessage.instanceFromNib()
                        errorView.show(message: response.errorMessage!)
                    }
                case .failure(let message):
                    print(message.body)
                        break
            }
        }
    }
    
    
    // MARK: - Helper    
    
    private func showAlert() {
        let alert = UIAlertController(title: "Проверьте почту",
            message: "Мы отправили ссылку для смены пароля на \(emailField.text!)", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Продолжить",
                                      style: UIAlertAction.Style.default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
