//
//  MPLFavouritesViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 11/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import Alamofire

class MPLFavouritesViewController: UITableViewController, MPLFavouritesCellOutput {
    
    // MARK: - Private vars & lets
    
    private var data: APIResponse.Wishlist?
    
    
    // MARK: - Init

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupInitialState()
        downloadFavourites()
    }
    
    private func setupInitialState() {
        setupClearButton()
        setupTableView()
        deleteFromWishlist(id: "")
    }
    
    private func setupClearButton() {
        let rightBarButtonItem = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(self.showAlert))
        rightBarButtonItem.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLFavouritesCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLFavouritesCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }
    

    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.products.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLFavouritesCell.cellIdentifier(), for: indexPath) as! MPLFavouritesCell
        let product = data!.products[indexPath.row]
        cell.configure(product: product)
        cell.layoutIfNeeded()
        cell.output = self
        MPLAppearance.addShadow(view: cell.frameView)
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLFavouritesCell.cellHeight()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    // MARK: - Network
    
    private func downloadFavourites() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.getWishlist,
                        params: ["client_token" : token]) {
                            (res: Swift.Result<APIResponse.Wishlist, AlertMessage>) in
                            switch res {
                            case .success(let response):
                                self.data = nil
                                self.data = response
                                self.tableView.reloadData()
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }

    private func deleteFromWishlist(id: String) {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.wishlistDelete,
                        params: ["client_token" : token, "product_id" : id]) {
                            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                            switch res {
                            case .success( _):
                                self.downloadFavourites()
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
    
    private func clearWishlist() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.clearWithlist,
                        params: ["client_token" : token]) {
                            (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                            switch res {
                            case .success( _):
                                self.downloadFavourites()
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
    
    
    // MARK: - Helper
    
    @objc func showAlert() {
        let alert = UIAlertController(title: nil,
                                      message: "Вы уверены, что хотите очистить весь список?",
            preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Отмена",
                                      style: UIAlertAction.Style.destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Очистить",
                                      style: UIAlertAction.Style.default, handler: { action in
                                        self.clearWishlist()
                                        self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - MPLFavouritesCellOutput
    
    func delete(id: String) {
        deleteFromWishlist(id: id)
    }
}
