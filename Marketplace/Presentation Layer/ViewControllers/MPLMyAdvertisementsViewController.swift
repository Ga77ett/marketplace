//
//  MPLMyAdvertisementsViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 16/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import Alamofire

class MPLMyAdvertisementsViewController: UITableViewController {
    
    // MARK: - Private vars & lets
    
    private var data: APIResponse.MyAdvertisements?
    

    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupInitialState()
        downloadAdvertisements()
    }
    
    private func setupInitialState() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLFavouritesCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLFavouritesCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }

    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.advertisements.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLFavouritesCell.cellIdentifier(), for: indexPath) as! MPLFavouritesCell
        let ad = data!.advertisements[indexPath.row]
        cell.configure(ad: ad)
        cell.layoutIfNeeded()
        MPLAppearance.addShadow(view: cell.frameView)
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLFavouritesCell.cellHeight()
    }

    
    // MARK: - Network
    
    private func downloadAdvertisements() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.myAdvertisements,
                        params: ["client_token" : token]) {
                            (res: Swift.Result<APIResponse.MyAdvertisements, AlertMessage>) in
                            switch res {
                            case .success(let response):
                                self.data = nil
                                self.data = response
                                self.tableView.reloadData()
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
}
