//
//  MPLChangePassViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 10/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class MPLChangePassViewController: UIViewController {

    // MARK: - Public vars & lets
    
    @IBOutlet weak var oldPassField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassAganField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassErrorLabel: UILabel!
    @IBOutlet weak var oldPassErrorLabel: UILabel!
    
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    // MARK: - Actions
    
    @IBAction func changePassAction(_ sender: Any) {
        guard let pass = oldPassField.text else { return }
        var oldPassIsValid = false
        if pass.isEmpty {
            oldPassErrorLabel.isHidden = false
            oldPassField.toogleErrorState(error: true)
            oldPassIsValid = false
            
        } else {
            oldPassErrorLabel.isHidden = true
            oldPassField.toogleErrorState(error: false)
            oldPassIsValid = true
        }
        
        guard let newPass = newPassField.text else { return }
        guard let newPassAgain = newPassAganField.text else { return }
        var newPassIsValid = false
        if newPass.isEmpty || newPassAgain != newPass {
            newPassErrorLabel.isHidden = false
            newPassField.toogleErrorState(error: true)
            newPassAganField.toogleErrorState(error: true)
            newPassIsValid = false
            
        } else {
            newPassErrorLabel.isHidden = true
            newPassField.toogleErrorState(error: false)
            newPassAganField.toogleErrorState(error: false)
            newPassIsValid = true
        }
        
        if newPassIsValid && oldPassIsValid {
            updatePass()
        }
    }
    
    @IBAction func secureAction(_ sender: Any) {
        oldPassField.isSecureTextEntry.toggle()
        newPassField.isSecureTextEntry.toggle()
        newPassAganField.isSecureTextEntry.toggle()
    }
    
    @IBAction func resetPassAction(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Authorization", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLResetPassViewController") as! MPLResetPassViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    // MARK: - Network
    
    private func updatePass() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.updatePass,
                        params: ["client_token" : token,
                                 "old_password" : oldPassField.text!,
                                 "password1" : newPassField.text!,
                                 "password2" : newPassAganField.text!]) {
                                    (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                                    switch res {
                                    case .success(let response):
                                        if response.errorCode == 0 {                                            self.navigationController?.popViewController(animated: true)
                                            
                                        } else {
                                            let errorView = MPLErrorMessage.instanceFromNib()
                                            errorView.show(message: response.errorMessage!)
                                        }
                                        
                                    case .failure(let message):
                                        print(message.body)
                                        break
                                    }
        }
    }
}
