//
//  MPLSimpleSearchViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 22/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

protocol MPLSimpleSearchViewControllerOutput {
    func done(filter: APIResponse.Filters.Filter)
}

class MPLSimpleSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    // MARK: - Public vars & lets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var doneButtonBottomConstraint: NSLayoutConstraint!
    public var output: MPLSimpleSearchViewControllerOutput?
    
    
    // MARK: - Private vars & lets
    private var data: APIResponse.Filters.Filter?
    private var initialData: APIResponse.Filters.Filter?
    
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    private func setupInitialState() {
        setupTableView()
        setupSearchBar()
        
        if data?.displayType == "radio" {
            doneButton.isHidden = true
            
        } else {
            setupButton()
        }
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: MPLSearchCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLSearchCell.cellIdentifier())
        
        tableView.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 50))
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
        
        searchBar.placeholder = "Поиск"
        searchBar.setImage(UIImage.init(named: ""), for: .bookmark, state: .normal)
        searchBar.searchBarStyle = .minimal
        searchBar.barTintColor = navigationController?.navigationBar.barTintColor
        searchBar.tintColor = self.view.tintColor
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Отменить"
        let attributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.foregroundColor : UIColor.darkBlueGrey,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .semibold)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }
    
    private func setupButton() {
        doneButton.layer.cornerRadius = 25
        doneButton.clipsToBounds = true
        MPLAppearance.addShadow(view: doneButton)
        
        doneButton.addTarget(self, action: #selector(doneDidTapped), for: .touchDown)
    }
    
    
    // MARK: - Data
    
    public func configure (filter: APIResponse.Filters.Filter, output: MPLSimpleSearchViewControllerOutput) {
        self.title = filter.name
        data = filter
        initialData = filter
        self.output = output
    }
    
    @objc func doneDidTapped() {
        guard let data = self.initialData else { return }
        output?.done(filter: data)
        navigationController?.popViewController(animated: true)
    }

    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.variants?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLSearchCell.cellIdentifier(), for: indexPath) as! MPLSearchCell
        
        guard let variant = data?.variants![indexPath.row] else { return cell }
        let single = data?.displayType == "checkbox" ? false: true
        cell.configure(variant: variant, single: single)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLSearchCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard var variant = data?.variants![indexPath.row] else { return }
        
        if data?.displayType == MPLFilterDisplayType.radio.rawValue {
            if let row = data?.variants!.firstIndex(where: {$0.selected == true}) {
                data?.variants![row].selected = false
            }
            
            if let row = initialData?.variants!.firstIndex(where: {$0.selected == true}) {
                initialData?.variants![row].selected = false
            }
        }
        
        variant.selected = !variant.selected
        data?.variants![indexPath.row] = variant
        let index = initialData!.variants!.firstIndex{$0.name == variant.name}
        initialData!.variants![index!] = variant
        
        if data?.displayType == MPLFilterDisplayType.radio.rawValue {
            doneDidTapped()
            
        } else {
            tableView.reloadData()
        }
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        
        guard let data = self.initialData else { return }
        self.data?.variants?.removeAll()
        self.data? = data
        tableView.reloadData()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let data = self.initialData else { return }
        guard let text = searchBar.text else { return }
        
        if (text.count > 0) {
            let filtered = data.variants!.filter { $0.name.uppercased().contains(text.uppercased()) }
            self.data?.variants = filtered
            
        } else {
            self.data?.variants!.removeAll()
            self.data? = data
        }
        
        tableView.reloadData()
    }
    
    
    // MARK: - Keyboard
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            
            let isKeyboardShowing = notification.name.rawValue == "UIKeyboardWillShowNotification"
            
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.doneButtonBottomConstraint.constant = isKeyboardShowing ? keyboardFrame!.height + 20 : 30
                self.view.layoutIfNeeded()
            })
        }
    }
}
