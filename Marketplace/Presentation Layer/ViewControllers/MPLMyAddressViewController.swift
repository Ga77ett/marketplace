//
//  MPLMyAddressViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 10/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class MPLMyAddressViewController: UIViewController, MPLCitiesModuleOutput {

    // MARK: - Public vars & lets
    
    @IBOutlet weak var addressField: SkyFloatingLabelTextField!
    @IBOutlet weak var zipField: SkyFloatingLabelTextField!
    @IBOutlet weak var cityField: SkyFloatingLabelTextField!
    public var output: MPLPersonalDataViewControllerOutput?
    
    
    // MARK: - Private vars & lets
    
    private var profile: APIResponse.Profile?
    
    
    // MARK: - Init
    
    public func configure(profile: APIResponse.Profile, output: MPLPersonalDataViewControllerOutput) {
        self.output = output
        self.profile = profile
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        setupInitialState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func setupInitialState() {
        setupFields()
        setupSaveButton()
    }
    
    private func setupFields() {
        guard let profile = self.profile else { return }
        DispatchQueue.main.async {
            self.cityField.text = profile.data.city
            self.addressField.text = profile.data.address
            self.zipField.text = profile.data.zipCode
        }
    }
    
    private func setupSaveButton() {
        let rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(saveAction))
        rightBarButtonItem.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    
    // MARK: - Actions

    @IBAction func cityAction(_ sender: Any) {
        showCities()
    }
    
    @objc func saveAction() {
        updateProfile()
    }
    
    private func showCities() {
        guard let cityID = profile!.data.cityID else { return }

        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLCitiesViewController") as! MPLCitiesViewController
            let presenter = controller.output as! MPLCitiesPresenter
            presenter.configure(cityID: String("\(cityID)"), output: self)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    // MARK: - MPLCitiesModuleOutput
    
    func citySelected(cityID: String, name: String) {
        profile?.data.cityID = Int(cityID)
        profile?.data.city = name
        
        DispatchQueue.main.async {
            self.cityField.text = name
        }
    }
    
    
    // MARK: - Network
    
    private func updateProfile() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.profileUpdate,
                        params: ["client_token" : token,
                                 "user_data[s_address]" : addressField.text ?? "",
                                 "user_data[s_city_id]" : profile!.data.cityID ?? "",
                                 "user_data[s_city]" : profile!.data.city ?? "",
                                 "user_data[s_zipcode]": zipField.text ?? ""]) {
                                    (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                                    switch res {
                                    case .success(let response):
                                        if response.errorCode == 0 {
                                            self.output?.dataChanged()
                                            self.navigationController?.popViewController(animated: true)
                                            
                                        } else {
                                            let errorView = MPLErrorMessage.instanceFromNib()
                                            errorView.show(message: response.errorMessage!)
                                        }
                                        
                                    case .failure(let message):
                                        print(message.body)
                                        break
                                    }
        }
    }
}
