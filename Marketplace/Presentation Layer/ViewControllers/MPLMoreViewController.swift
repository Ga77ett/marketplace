//
//  MPLMoreViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 05/08/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLMoreViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
    }
    
    private func setupInitialState() {
        setupTableView()
    }

    private func setupTableView() {
        tableView.register(UINib(nibName: MPLInfoCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLInfoCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLInfoCell.cellIdentifier(), for: indexPath) as! MPLInfoCell
        
        switch indexPath.row {
        case 0:
            cell.configureCell(type: .info)
        case 1:
            cell.configureCell(type: .feedback)
        case 2:
            cell.configureCell(type: .settings)
        case 3:
            cell.configureCell(type: .about)
        default:
            break;
        }
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLInfoCell.cellHeight()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 2:
            showSettings()
        default:
            break
        }
    }
    
    
    // MARK: - Routes
    
    private func showSettings() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MPLSettingsViewController") as! MPLSettingsViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
