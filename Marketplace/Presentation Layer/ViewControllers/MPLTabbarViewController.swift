//
//  MPLTabbarViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 03/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit

class MPLTabbarViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
    }
    
    
    // MARK: - UITabBarControllerDelegate
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index = tabBarController.selectedIndex
        if index == 3 {
            
        }
    }
}
