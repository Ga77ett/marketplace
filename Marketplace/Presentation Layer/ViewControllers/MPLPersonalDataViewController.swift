//
//  MPLPersonalDataViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 10/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

protocol MPLPersonalDataViewControllerOutput {
    func dataChanged()
}

class MPLPersonalDataViewController: UIViewController {
    
    // MARK: - Public vars & lets

    @IBOutlet weak var nameField: SkyFloatingLabelTextField!
    @IBOutlet weak var nameErrorLabel: UILabel!
    @IBOutlet weak var lastNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    public var output: MPLPersonalDataViewControllerOutput?
    
    
    // MARK: - Private vars & lets
    
    private var profile: APIResponse.Profile?
    
    
    // MARK: - Init
    
    public func configure(profile: APIResponse.Profile, output: MPLPersonalDataViewControllerOutput) {
        self.profile = profile
        self.output = output
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        setupInitialState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func setupInitialState() {
        setupFields()
        setupSaveButton()
    }
    
    private func setupFields() {
        guard let profile = self.profile else { return }
        DispatchQueue.main.async {
            self.nameField.text = profile.data.firstName
            self.lastNameField.text = profile.data.lastName
            self.emailField.text = profile.data.email
            self.phoneField.text = profile.data.phone
            
            self.emailField.textColor = self.emailField.placeholderColor
            self.phoneField.textColor = self.phoneField.placeholderColor
        }
    }
    
    private func setupSaveButton() {
        let rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .plain, target: self, action: #selector(saveAction))
        rightBarButtonItem.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    
    // MARK: Action

    @IBAction func changePassAction(_ sender: Any) {
    }
    
    @objc func saveAction() {
        guard let name = nameField.text else {
            nameErrorLabel.isHidden = false
            return
        }
        
        if name.isEmpty {
            nameErrorLabel.isHidden = false
            
        } else {
            nameErrorLabel.isHidden = true
            updateProfile()
        }
    }
    
    
    // MARK: - Network
    
    private func updateProfile() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.profileUpdate,
                        params: ["client_token" : token,
                                 "user_data[firstname]" : nameField.text!,
                                 "user_data[lastname]" : lastNameField.text!]) {
                                    (res: Swift.Result<APIResponse.SimpleResponse, AlertMessage>) in
                                    switch res {
                                    case .success(let response):
                                        if response.errorCode == 0 {
                                            self.output?.dataChanged()
                                            self.navigationController?.popViewController(animated: true)
                                            
                                        } else {
                                            let errorView = MPLErrorMessage.instanceFromNib()
                                            errorView.show(message: response.errorMessage!)
                                        }
                                        
                                    case .failure(let message):
                                        print(message.body)
                                        break
                                    }
        }
    }
}
