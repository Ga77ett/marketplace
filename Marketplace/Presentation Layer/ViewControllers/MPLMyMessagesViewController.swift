//
//  MPLMyMessagesViewController.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 17/09/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import UIKit
import Alamofire

class MPLMyMessagesViewController: UITableViewController {
    
    // MARK: - Private vars & lets
    
    private var data: APIResponse.ChatList?
    

    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        setupInitialState()
        downloadChatList()
    }
    
    private func setupInitialState() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: MPLMyMessageCell.cellNibName(), bundle: nil),
                           forCellReuseIdentifier: MPLMyMessageCell.cellIdentifier())
        
        tableView.tableFooterView = UIView()
    }

    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.threads.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MPLMyMessageCell.cellIdentifier(), for: indexPath) as! MPLMyMessageCell
        let thread = data!.threads[indexPath.row]
        cell.configure(thread: thread)
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MPLMyMessageCell.cellHeight()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let thread = data!.threads[indexPath.row]
        showChat(thread: thread)
    }
    
    
    // MARK: - Network
    
    private func downloadChatList() {
        let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
        guard let token = UserDefaults.standard.object(forKey: kMPLUserToken) else { return }
        apiManager.call(type: RequestItemsType.getChatList,
                        params: ["client_token" : token]) {
                            (res: Swift.Result<APIResponse.ChatList, AlertMessage>) in
                            switch res {
                            case .success(let response):
                                self.data = nil
                                self.data = response
                                self.tableView.reloadData()
                            case .failure(let message):
                                print(message.body)
                                break
                            }
        }
    }
    
    
    // MARK: - Route
    
    func showChat(thread: APIResponse.ChatList.Thread) {
        DispatchQueue.main.async {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "MPLChatViewController") as! MPLChatViewController
            viewController.thread = thread
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
