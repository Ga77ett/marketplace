//
//  NetworkItems.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 18/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Alamofire

// MARK: - Enums

enum NetworkEnvironment {
    case dev
    case production
    case stage
}

enum RequestItemsType {
    case banners
    case categories
    case specialProducts
    case popularBrands
    case productList
    case filters
    case cities
    case resetPass
    case createProfile
    case auth
    case getProfile
    case profileUpdate
    case updatePass
    case getWishlist
    case wishlistDelete
    case clearWithlist
    case wishlistAdd
    case myAdvertisements
    case getChatList
    case getChat
    case sendMessage
}

// MARK: - Extensions
// MARK: - EndPointType

extension RequestItemsType: EndPointType {
    
    // MARK: - Vars & Lets
    
    var baseURL: String {
        switch APIManager.networkEnviroment {
        case .dev: return "http://cs.portal-korablik.ru/custom_api"
        case .production: return "http://cs.portal-korablik.ru/custom_api"
        case .stage: return "http://cs.portal-korablik.ru/custom_api"
        }
    }
    
    var version: String {
        return "/1.0"
    }
    
    var path: String {
        switch self {
        case .banners:
            return "/banners"
            
        case .categories:
            return "/categories"
            
        case .specialProducts:
            return "/special_products"
            
        case .popularBrands:
            return "/brands"
            
        case .productList:
            return "/custom_products"
            
        case .filters:
            return "/filters"
            
        case .cities:
            return "/cities"
            
        case .resetPass:
            return "/auth_recover_password"
            
        case .createProfile:
            return "/profiles_add"
            
        case .auth:
            return "/custom_auth_tokens"
            
        case .getProfile:
            return "/get_profiles"
            
        case .profileUpdate:
            return "/profiles_update"
            
        case .updatePass:
            return "/change_password"
            
        case .getWishlist:
            return "/get_wishlist"
            
        case .wishlistDelete:
            return "/wishlist_delete"
            
        case .clearWithlist:
            return "/clear_wishlist"
            
        case .wishlistAdd:
            return "/wishlist_add"
            
        case .myAdvertisements:
            return "/get_my_advertisements"
            
        case .getChatList:
            return "/get_chat_list"
            
        case .getChat:
            return "/get_chat_details"
            
        case .sendMessage:
            return "/send_communication_message"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .banners, .categories, .specialProducts, .popularBrands, .productList, .filters, .cities, .resetPass, .getProfile, .getWishlist, .myAdvertisements, .getChatList, .getChat:
            return .get
            
        case .createProfile, .auth, .profileUpdate, .updatePass, .wishlistDelete, .clearWithlist, .wishlistAdd, .sendMessage:
            return .post
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Api-Token": "a270c2ca04cb4406a1e2227d70830e43"]
            // для пост Content-Type: application/x-www-form-urlencoded
        }
    }
    
    var url: URL {
        switch self {
        default:
            return URL(string: self.baseURL + self.version + self.path)!
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        default:
            return URLEncoding.methodDependent
        }
    }
    
}
