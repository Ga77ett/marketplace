//
//  MPLResponseStruct.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 19/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

enum APIResponse {
    
    // MARK: - Banners
    
    struct BannerList: Codable {
        let banners: [Banner]
    }
    
    struct Banner: Codable {
        struct MainPair: Codable {
            struct Icon: Codable {
                enum CodingKeys: String, CodingKey {
                    case imagePath  = "image_path"
                }
                
                let imagePath: String
            }
            
            let icon: Icon
        }
        
        let bannerID: String
        let mainPair: MainPair
        
        enum CodingKeys: String, CodingKey {
            case bannerID  = "banner_id"
            case mainPair  = "main_pair"
        }
    }
    
    
    // MARK: - Categories
    
    struct CategoriesList: Codable {
        var categories: [Category]
    }
    
    struct Category: Codable {
        var categoryID: String
        var parentID: String
        var name: String
        var hasChildren: Bool
        var productCount: String
        var subcatsCount: Int
        let mainPair: MainPair?
        
        enum CodingKeys: String, CodingKey {
            case categoryID     = "category_id"
            case mainPair       = "main_pair"
            case parentID       = "parent_id"
            case hasChildren    = "category_has_children"
            case productCount   = "product_count"
            case subcatsCount   = "product_count_subcats"
            case name           = "category"
        }
        
        struct MainPair: Codable {
            enum CodingKeys: String, CodingKey {
                case detailed
            }
            
            let detailed: Detailed
            
            struct Detailed: Codable {
                enum CodingKeys: String, CodingKey {
                    case imagePath  = "image_path"
                }
                
                let imagePath: String
            }
        }
    }
    
    
    // MARK: - Special product
    
    struct SpecialProducts: Codable {
        let products: [Product]
    }
    
    
    // MARK: - Brands
    
    struct BrandList: Codable {
        let brands: [Brand]
        
        struct Brand: Codable {
            let imagePair: ImagePair
            let featureID: String
            let variantID: String
            let name: String
            
            enum CodingKeys: String, CodingKey {
                case imagePair  = "image_pair"
                case featureID  = "feature_id"
                case variantID  = "variant_id"
                case name       = "variant"
            }
            
            struct ImagePair: Codable {
                let icon: Icon
                
                struct Icon:Codable {
                    let imagePath: String
                    
                    enum CodingKeys: String, CodingKey {
                        case imagePath  = "image_path"
                    }
                }
            }
        }
    }
    
    
    // MARK: - Product list
    
    struct Product: Codable {
        let id: String
        let name: String
        let price: Float
        let oldPrice: Float
        let imagePath: String?
        let discount: String?
        var inWishlist: Bool? = false
        let subtype: String
        let timestamp: String
        let images: [Image]?
        let description: String?
        let averageRating: Int?
        let categoryName: String
        let companyName: String?
        let sellerName: String?
        let productCode: String?
        
        enum CodingKeys: String, CodingKey {
            case id             = "product_id"
            case name           = "product"
            case price          = "price"
            case oldPrice       = "list_price"
            case discount       = "list_discount_prc"
            case imagePath      = "preview_picture"
            case inWishlist     = "in_wishlist"
            case subtype        = "product_subtype"
            case timestamp      = "timestamp"
            case images         = "all_images"
            case description    = "full_description_text"
            case averageRating  = "average_rating"
            case categoryName   = "main_category_name"
            case companyName    = "company_name"
            case sellerName     = "user_firstname"
            case productCode    = "product_code"
        }
        
        struct Image: Codable {
            let largeURL: String
            let smallURL: String
            
            enum CodingKeys: String, CodingKey {
                case largeURL = "full_detailed_url"
                case smallURL = "small_detailed_url"
            }
        }
    }
    
    struct ProductList: Codable {
        var products: [Product]
        let params: Params
        
        struct Params: Codable {
            let totalItems: String
            
            enum CodingKeys: String, CodingKey {
                case totalItems = "total_items"
            }
        }
    }
    
    
    // MARK: - Filters
    
    struct Filters: Codable {
        var filters: [Filter]
        
        struct Filter: Codable {
            let id: String
            let name: String
            let displayType: String
            var variants: [Variant]?
            let min: Int?
            let max: Int?
            var selectedMin: Int = 0
            var selectedMax: Int = 0
            var switchOn: Bool = false

            enum CodingKeys: String, CodingKey {
                case id             = "filter_id"
                case displayType    = "mobile_app_display_type"
                case name           = "filter"
                case variants
                case min
                case max
            }
            
            struct Variant: Codable {
                let id: Int
                let name: String
                var selected: Bool = false
                
                enum CodingKeys: String, CodingKey {
                    case id    = "variant_id"
                    case name  = "variant"
                }
            }
        }
    }
    
    
    // MARK: - Cities
    
    struct Cities: Codable {
        var list: [City]
        
        enum CodingKeys: String, CodingKey {
            case list = "cities"
        }
        
        struct City: Codable {
            let id: String
            let name: String
            var selected: Bool = false
            
            enum CodingKeys: String, CodingKey {
                case id    = "city_id"
                case name  = "city"
            }
        }
    }
    
    
    // MARK: - Reset password, create profile, auth
    
    struct SimpleResponse: Codable {
        let errorCode: Int
        let errorMessage: String?
        let token: String?
        
        enum CodingKeys: String, CodingKey {
            case errorCode      = "error_code"
            case errorMessage   = "error_message"
            case token
        }
    }
    
    
    // MARK: - Profile
    
    struct Profile: Codable {
        var data: UserData
        
        enum CodingKeys: String, CodingKey {
            case data = "user_data"
        }
        
        struct UserData: Codable {
            let email: String
            let phone: String
            let firstName: String?
            let lastName: String?
            var city: String?
            var cityID: Int?
            var address: String?
            var zipCode: String?
            
            enum CodingKeys: String, CodingKey {
                case email = "email"
                case phone = "phone"
                case city = "s_city"
                case cityID = "s_city_id"
                case address = "s_address"
                case zipCode = "s_zipcode"
                case firstName = "firstname"
                case lastName = "lastname"
            }
        }
    }
    
    
    // MARK: - Wishlist
    
    struct Wishlist: Codable {
        let products: [Product]
    }
    
    
    // MARK: - My advertisements
    
    struct MyAdvertisements: Codable {
        let advertisements: [Advertisements]
        
        struct Advertisements: Codable {
            let id: String
            let name: String
            let price: Float
            let oldPrice: Float
            let imagePath: String?
            let statusText: String
            let discount: String?
            
            enum CodingKeys: String, CodingKey {
                case id             = "product_id"
                case name           = "product"
                case price          = "price"
                case oldPrice       = "list_price"
                case imagePath      = "preview_picture"
                case statusText     = "general_status_text"
                case discount       = "list_discount_prc"
            }
        }
    }
    
    
    // MARK: - Chat list
    
    struct ChatList: Codable {
        let threads: [Thread]
        
        struct Thread: Codable {
            let firstName: String
            let lastName: String?
            let productName: String?
            let lastMessage: String
            let lastUpdated: String
            let id: String
            let objectID: String
            let objectType: String
            
            enum CodingKeys: String, CodingKey {
                case firstName   = "firstname"
                case lastName    = "lastname"
                case productName = "object_name"
                case lastMessage = "last_message"
                case lastUpdated = "last_updated"
                case id          = "thread_id"
                case objectID    = "object_id"
                case objectType  = "object_type"
            }
        }
    }
    
    
    // MARK: - Chat
    
    struct Chat: Codable {
        var messages: [Message]
        
        struct Message: Codable {
            let isMyMessage: Bool
            let message: String
            let timestamp: String
            
            enum CodingKeys: String, CodingKey {
                case isMyMessage = "is_my_message"
                case message     = "message"
                case timestamp   = "timestamp"
            }
        }
    }
}
