//
//  EndPointType.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 18/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Alamofire

protocol EndPointType {
    
    // MARK: - Vars & Lets
    
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var url: URL { get }
    var encoding: ParameterEncoding { get }
    var version: String { get }
    
}
