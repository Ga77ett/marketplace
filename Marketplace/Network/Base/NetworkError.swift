//
//  NetworkError.swift
//  Marketplace
//
//  Created by Kirill Shalankin on 18/06/2019.
//  Copyright © 2019 Korablik. All rights reserved.
//

import Foundation

class NetworkError: Codable {
    
    let message: String
    let key: String?
    
}
